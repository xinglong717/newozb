﻿using Microsoft.EntityFrameworkCore;


namespace ozb.Models
{
    public partial class BlogDBContext: DbContext
    {
        public BlogDBContext()
        {
        }

        public BlogDBContext(DbContextOptions<BlogDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Category> Category { get; set; }
        public virtual DbSet<Post> Post { get; set; }
        public virtual DbSet<Deal> Deal { get; set; }
        public virtual DbSet<User> User { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Name)
                    .HasColumnName("NAME")
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Slug)
                    .HasColumnName("SLUG")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Post>(entity =>
            {
                entity.Property(e => e.PostId).HasColumnName("POST_ID");

                entity.Property(e => e.CategoryId).HasColumnName("CATEGORY_ID");

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("CREATED_DATE")
                    .HasColumnType("datetime");

                entity.Property(e => e.Description)
                    .HasColumnName("DESCRIPTION")
                    .IsUnicode(false);

                entity.Property(e => e.Title)
                    .HasColumnName("TITLE")
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.Post)
                    .HasForeignKey(d => d.CategoryId)
                    .HasConstraintName("FK__Post__CATEGORY_I__1273C1CD");
            });
            
            modelBuilder.Entity<Deal>(entity =>
            {
                entity.Property(e => e.DealId).HasColumnName("DEAL_ID");
                entity.Property(e => e.CategoryId).HasColumnName("Category_ID");
                entity.Property(e => e.UserId).HasColumnName("User_ID");
                entity.Property(e => e.LinkedURL).HasColumnName("LinkedURL");
                entity.Property(e => e.CouponCode).HasColumnName("CouponCode");
                entity.Property(e => e.StartDate)
                   .HasColumnName("StartDate")
                   .HasColumnType("datetime");
                entity.Property(e => e.EndDate)
                   .HasColumnName("EndDate")
                   .HasColumnType("datetime");
                entity.Property(e => e.Title)
                    .HasColumnName("TITLE")
                    .HasMaxLength(2000)
                    .IsUnicode(false);
                entity.Property(e => e.IsFreebie).HasColumnName("IsFreebie");
                entity.Property(e => e.TaggingText).HasColumnName("TaggingText");
                entity.Property(e => e.DescriptionText).HasColumnName("DescriptionText");
                entity.Property(e => e.AmountVotes).HasColumnName("AmountVotes");
            });
            modelBuilder.Entity<User>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");
                entity.Property(e => e.FirstName).HasColumnName("FirstName");
                entity.Property(e => e.LastName).HasColumnName("LastName");
                entity.Property(e => e.eMail).HasColumnName("eMail");
                entity.Property(e => e.Password).HasColumnName("PassWord");
            });
        }
    }
}
