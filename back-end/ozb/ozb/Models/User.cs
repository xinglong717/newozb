
using System;
using System.Collections.Generic;


namespace ozb.Models
{
    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string eMail { get; set; }
        public string Password { get; set; }
    }
}