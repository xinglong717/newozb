﻿using Microsoft.EntityFrameworkCore;
using ozb.Models;
using ozb.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ozb.Repository
{
    public class DealRepository:IDealRepository
    {
        BlogDBContext db;
        public DealRepository(BlogDBContext _db)
        {
            db = _db;
        }

        public async Task<List<Category>> GetCategories()
        {
            if (db != null)
            {
                return await db.Category.ToListAsync();
            }

            return null;
        }

        public async Task<List<Deal>> GetDeals()
        {
            if (db != null)
            {
                return await db.Deal.ToListAsync();
            }

            return null;
        }
        public async Task<int> AddDeal(Deal deal)
        {
            if (db != null)
            {
                await db.Deal.AddAsync(deal);
                await db.SaveChangesAsync();

                return deal.DealId;
            }

            return 0;
        }


        public async Task<Deal> GetDeal(int? dealId)
        {
            if (db != null)
            {
                return await (from d in db.Deal
                              where d.DealId == dealId
                              select new Deal
                              {
                                  DealId = d.DealId,
                                  CategoryId = d.CategoryId,
                                  UserId = d.UserId,
                                  Title = d.Title,
                                  LinkedURL = d.LinkedURL,
                                  CouponCode = d.CouponCode,
                                  StartDate = d.StartDate,
                                  EndDate = d.EndDate,
                                  IsFreebie = d.IsFreebie,
                                  DescriptionText = d.DescriptionText,
                                  AmountVotes = d.AmountVotes,
                              }).FirstOrDefaultAsync();
            }

            return null;
        }

        public async Task<int> DeleteDeal(int? dealId)
        {
            int result = 0;

            if (db != null)
            {
                //Find the post for specific post id
                var deal = await db.Deal.FirstOrDefaultAsync(x => x.DealId == dealId);

                if (deal != null)
                {
                    //Delete that post
                    db.Deal.Remove(deal);

                    //Commit the transaction
                    result = await db.SaveChangesAsync();
                }
                return result;
            }

            return result;
        }
        public async Task UpdateDeal(Deal deal)
        {
            if (db != null)
            {
                //Delete that post
                db.Deal.Update(deal);

                //Commit the transaction
                await db.SaveChangesAsync();
            }
        }

    }
}
