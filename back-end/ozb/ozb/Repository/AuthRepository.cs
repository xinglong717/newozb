using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ozb.Models;
using ozb.Repository.Interface;

namespace ozb.Repository
{
    public class AuthRepository : IAuthRepository
    {
        BlogDBContext db;

        public AuthRepository()
        {
        }

        public AuthRepository(BlogDBContext _db)
        {
            db = _db;
        }

        public async Task<List<User>> GetUsers()
        {
            if (db != null)
            {
                return await db.User.ToListAsync();
            }

            return null;
        }

        public async Task<User> GetUser(string eMail, string password){

            if (db != null)
            {
                return await (from d in db.User
                              where d.eMail == eMail
                              select new User
                              {
                                  Id = d.Id,
                                  FirstName = d.FirstName,
                                  LastName = d.LastName,
                                  eMail = d.eMail,
                                  Password = d.Password,
                              }).FirstOrDefaultAsync();
            }

            return null;
        }
    }
}