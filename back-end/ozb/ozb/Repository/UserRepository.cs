﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ozb.Models;

namespace ozb.Repository
{
    public class UserRepository:IUserRepository
    {
        BlogDBContext db;
        public UserRepository(BlogDBContext _db)
        {
            db = _db;
        }

        public async Task<List<User>> GetUsers()
        {
            if (db != null)
            {
                return await db.User.ToListAsync();
            }

            return null;
        }

        public async Task<int> AddUser(User user)
        {
            if (db != null)
            {
                await db.User.AddAsync(user);
                await db.SaveChangesAsync();

                return user.Id;
            }

            return 0;
        }


        public async Task<User> GetUser(int? userId)
        {
            if (db != null)
            {
                return await (from d in db.User
                              where d.Id == userId
                              select new User
                              {
                                  Id = d.Id,
                                  FirstName = d.FirstName,
                                  LastName = d.LastName,
                                  eMail = d.eMail,
                                  Password = d.Password,
                              }).FirstOrDefaultAsync();
            }

            return null;
        }

        public async Task<int> DeleteUser(int? userId)
        {
            int result = 0;

            if (db != null)
            {
                //Find the post for specific post id
                var user = await db.User.FirstOrDefaultAsync(x => x.Id == userId);

                if (user != null)
                {
                    //Delete that post
                    db.User.Remove(user);

                    //Commit the transaction
                    result = await db.SaveChangesAsync();
                }
                return result;
            }

            return result;
        }
        public async Task UpdateUser(User user)
        {
            if (db != null)
            {
                //Delete that post
                db.User.Update(user);

                //Commit the transaction
                await db.SaveChangesAsync();
            }
        }
    }
}
