using System.Collections.Generic;
using System.Threading.Tasks;
using ozb.Models;

namespace ozb.Repository.Interface
{
    public interface IAuthRepository
    {
        Task<List<User>> GetUsers();

        Task<User> GetUser(string login, string password);
    }
}