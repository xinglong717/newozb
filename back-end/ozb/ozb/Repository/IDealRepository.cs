﻿using ozb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ozb.Repository
{
    public interface IDealRepository
    {
        Task<List<Category>> GetCategories();
        Task<List<Deal>> GetDeals();
        Task<Deal> GetDeal(int? dealId);
        Task<int> AddDeal(Deal deal);
        Task<int> DeleteDeal(int? dealId);

        Task UpdateDeal(Deal deal);
    }
}
