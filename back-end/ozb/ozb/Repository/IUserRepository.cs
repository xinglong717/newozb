﻿using ozb.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ozb.Repository
{
    public  interface IUserRepository
    {
        Task<List<User>> GetUsers();
        Task<int> AddUser(User User);
        Task<User> GetUser(int? userId);
        Task<int> DeleteUser(int? userId);

        Task UpdateUser(User User);
    }
}
