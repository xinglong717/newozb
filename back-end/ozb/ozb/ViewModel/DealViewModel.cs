﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ozb.ViewModel
{
    public class DealViewModel
    {
        public int DealId { get; set; }
        public int CategoryId { get; set; }
        public int UserId { get; set; }
        public string Title { get; set; }
        public string LinkedURL { get; set; }
        public int CouponCode { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool IsFreebie { get; set; }
        public string TaggingText { get; set; }
        public string DescriptionText { get; set; }
        public int AmountVotes { get; set; }
    }
}
