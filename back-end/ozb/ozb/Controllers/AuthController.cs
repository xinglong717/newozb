﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ozb.Infra;
using ozb.Models;
using ozb.Repository.Interface;
using ozb.Controllers;

namespace ozb.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class AuthController : Controller
    {
        private readonly IAuthRepository _AuthRepository;

        public AuthController(IAuthRepository AuthRepository)
        {
            _AuthRepository = AuthRepository;
        }

        // POST api/Auth
        [HttpPost]
        public async Task<IActionResult> Create([FromBody]User user)
        {
            var usr = await _AuthRepository.GetUser(user.eMail, user.Password);

            if (usr != null)
            {

                var token = new JwtTokenBuilder()
                                    .AddSecurityKey(JwtSecurityKey.Create("key-value-token-expires"))
                                    .AddSubject(user.eMail)
                                    .AddIssuer("issuerTest")
                                    .AddAudience("bearerTest")
                                    .AddClaim("MembershipId", "111")
                                    .AddExpiry(1)
                                    .Build();

                return Ok(new
                {
                    FirstName = usr.FirstName,
                    LastName = usr.LastName,
                    Token = token.Value
                });

            }
            else
                return Unauthorized();
        }
    }
}
