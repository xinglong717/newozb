﻿using Microsoft.AspNetCore.Mvc;
using ozb.Models;
using ozb.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ozb.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DealController : ControllerBase
    {
        IDealRepository dealRepository;
        public DealController(IDealRepository _dealRepository)
        {
            dealRepository = _dealRepository;
        }


        [HttpGet]
        [Route("GetCategories")]
        public async Task<IActionResult> GetCategories()
        {
            try
            {
                var categories = await dealRepository.GetCategories();
                if (categories == null)
                {
                    return NotFound();
                }

                return Ok(categories);
            }
            catch (Exception)
            {
                return BadRequest();
            }

        }

        [HttpGet]
        [Route("GetDeals")]
        public async Task<IActionResult>GetDeals()
        {
            try
            {
                var deals = await dealRepository.GetDeals();
                if (deals == null)
                {
                    return NotFound();
                }

                return Ok(deals);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet]
        [Route("GetDeal")]
        public async Task<IActionResult> GetDeal(int? dealId)
        {
            if (dealId == null)
            {
                return BadRequest();
            }

            try
            {
                var deal = await dealRepository.GetDeal(dealId);

                if (deal == null)
                {
                    return NotFound();
                }

                return Ok(deal);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpPost]
        [Route("AddDeal")]
        public async Task<IActionResult> AddDeal([FromBody]Deal model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    var postId = await dealRepository.AddDeal(model);
                    if (postId > 0)
                    {
                        return Ok("OK!");
                    }
                    else
                    {
                        return NotFound();
                    }
                }
                catch (Exception)
                {

                    return BadRequest();
                }

            }

            return BadRequest();
        }

        [HttpDelete]
        [Route("DeleteDeal")]
        public async Task<IActionResult> DeleteDeal(int? dealId)
        {
            int result = 0;

            if (dealId == null)
            {
                return BadRequest();
            }

            try
            {
                result = await dealRepository.DeleteDeal(dealId);
                if (result == 0)
                {
                    return NotFound();
                }
                return Ok();
            }
            catch (Exception)
            {

                return BadRequest();
            }
        }

        [HttpPut]
        [Route("UpdateDeal")]
        public async Task<IActionResult> UpdateDeal([FromBody]Deal model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    await dealRepository.UpdateDeal(model);

                    return Ok();
                }
                catch (Exception ex)
                {
                    if (ex.GetType().FullName ==
                             "Microsoft.EntityFrameworkCore.DbUpdateConcurrencyException")
                    {
                        return NotFound();
                    }

                    return BadRequest();
                }
            }

            return BadRequest();
        }
    }
}
