
// @material-ui/icons

import Dashboard from "@material-ui/icons/Dashboard";
import Person from "@material-ui/icons/Person";
import Shopping_Cart from "@material-ui/icons/ShoppingCart";
import SupervisorAccount from "@material-ui/icons/SupervisorAccount";
import Fastfood from "@material-ui/icons/Fastfood";

// core components/views for Admin layout
import DashboardPage from "views/Dashboard/Dashboard.jsx";
import UserProfile from "views/UserProfile/UserProfile.jsx";
import DealsList from "views/DealsList/DealsList.jsx";
import SellerList from "views/SellerList/SellerList.jsx";
import ShoppingCart from "views/ShoppingCart/ShoppingCart.jsx";
import Foods from "views/Foods/Foods.jsx";

const dashboardRoutes = [
  // {
  //   path: "/main",
  //   name: "Dashboard",
  //   rtlName: "لوحة القيادة",
  //   icon: Dashboard,
  //   component: DashboardPage,
  //   layout: "/client"
  // },
  {
    path: "/dashboard",
    name: "Dashboard",
    rtlName: "لوحة القيادة",
    icon: Dashboard,
    component: DashboardPage,
    layout: "/admin"
  },
  {
    path: "/user",
    name: "User Profile",
    rtlName: "ملف تعريفي للمستخدم",
    icon: Person,
    component: UserProfile,
    layout: "/admin"
  },
  {
    path: "/deals",
    name: "Deals",
    icon: "content_paste",
    component: DealsList,
    layout: "/admin"
  },
  {
    path: "/seller",
    name: "Sellers",
    icon: SupervisorAccount,
    component: SellerList,
    layout: "/admin"
  },
  {
    path: "/cart",
    name: "Shopping Cart",
    icon: Shopping_Cart,
    component: ShoppingCart,
    layout: "/admin"
  },
  {
    path: "/foods",
    name: "Foods",
    icon: Fastfood,
    component: Foods,
    layout: "/admin"
  }
];

export default dashboardRoutes;
