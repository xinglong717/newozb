// import React from "react";
// import ReactDOM from "react-dom";
// import { createBrowserHistory } from "history";
// import { Router, Route, Switch, Redirect } from "react-router-dom";

// // core components
// import Admin from "layouts/Admin.jsx";
// import RTL from "layouts/RTL.jsx";
// // import Home from "client/Home.jsx";
// import "assets/css/material-dashboard-react.css?v=1.6.0";

// const hist = createBrowserHistory();

// ReactDOM.render(
//   <Router history={hist}>
//     <Switch>
//       <Route path="/admin" component={Admin} />
//       <Route path="/rtl" component={RTL} />
//       {/* <Route exact path="/" component={Home} /> */}
//       <Redirect from="/admin" to="/admin/dashboard" />
//     </Switch>
//   </Router>,
//   document.getElementById("root")
// );

import React, { Component } from 'react';
import { createBrowserHistory } from "history";
import { Router, Route, Switch } from "react-router-dom";
import "assets/css/material-dashboard-react.css?v=1.6.0";

import indexRoutes from "./routes.jsx";

const hist = createBrowserHistory();

class Admin extends Component {   
  render() {
    return (
      <Router history={hist}>
      <Switch>
        {indexRoutes.map((prop, key) => {
          return <Route path={prop.path} component={prop.component} key={key} />;
        })}
      </Switch>
    </Router>
    )
  }
}

export default Admin;
