import React from 'react';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { withStyles } from '@material-ui/core/styles';
import TopBar from './components/TopBar';

import SubNavBar from './components/SubNavBar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Paper from '@material-ui/core/Paper';


  
const useStyles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper:{
    marginTop: theme.spacing(2),
  },
  SubNavBarMenu:{
    cursor: 'pointer',
    color:`white`,
    fontWeight:`bold`,
    backgroundColor:`black`,
  },
});

const mapStateToProps = state => {
  return{ }
};

function mapDispatchToProps(dispatch) {
  return{}
}

class ConnectedHome extends React.Component{
  
  state = { }


  render(){
    const {classes} = this.props;
    
    return (
      <React.Fragment>  
          <TopBar/>
          <div className = {classes.SubNavBarMenu}>
          <Container maxWidth = "xl" >
            <Grid container
                direction="row"
                justify="flex-start"
                alignItems="flex-end"
            >
              <Grid item xs = {12} md = {1}></Grid>
              <Grid item xs = {12} md = {8} >
                <SubNavBar/>
              </Grid>
            </Grid>
          </Container>
        </div>
        <CssBaseline />
          <Container maxWidth = "xl" style = {{paddingLeft:'150px',paddingRight:'150px'}}>
            <Paper elevation = {0} className = {classes.paper}>
              {/* {this.props.showMainDeal?<MainDeals/>:null}
              {this.props.showSubmitDeal?<SubmitDeal/>:null} */}
            </Paper>  
          </Container>      
      </React.Fragment>
    );
  }
}

ConnectedHome.propTypes = {
  classes: PropTypes.object.isRequired
};
const Home = connect(mapStateToProps,mapDispatchToProps)(ConnectedHome);
export default withStyles(useStyles)(Home)
