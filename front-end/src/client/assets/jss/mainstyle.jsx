import { makeStyles } from '@material-ui/core/styles';

const mainStyles = makeStyles(theme => ({
    root: {
      flexGrow: 1,
    },
    avatar: {
      margin: 10,
      width: 50,
      height: 50,
    },
    button: {
      margin: theme.spacing(1),
    },
    card: {
      maxWidth: 345,
    },
    couponbg:{
      backgroundColor: `#FF0000`,
    },
    icon: {
      margin: theme.spacing(1),
      fontSize: 32,
    },
    media: {
      height: theme.spacing(12),
    },
    paper:{
      marginTop:theme.spacing(1),
    },
    couponpaper:{
      margin:theme.spacing(2),
    }
  }));

  export default mainStyles;