import React from 'react';
import {connect} from 'react-redux';    
import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import Divider from '@material-ui/core/Divider';

import ViewIcon from '@material-ui/icons/ChatBubbleSharp';


const useStyles = theme => ({
    root: {
      flexGrow: 1,
      backgroundColor: theme.palette.background.paper,
    },
    eachItem:{
        '&:hover': {
            background: "#ffffff",
         },
        backgroundColor:`#fffde7`
    },
    itemText:{
        marginLeft:theme.spacing(1),
    },
    mainTitle:{
        marginTop:theme.spacing(2),
        padding:theme.spacing(1),
        fontWeight:`bold`,
        backgroundColor:`#ffe082`
    },
    votedAmount:{
        width:theme.spacing(6),
        marginLeft:theme.spacing(2),
        borderRadius: theme.spacing(0.5),
        fontFamily:`Arial, sans-serif`,
        fontWeight:`bold`,
        background: `#388e3c`,
        color:`#ffffff`,
    },
    viewIcon:{
        height:theme.spacing(2),
        width:theme.spacing(2),
        color:`grey`
    }
  });

  function mapDispatchToProps(dispatch) {
    return{
     
    };
  }

  const mapStateToProps = state => {
    return { 
            
    };
  };


class ConnectedFrontDeals extends React.Component{
    constructor(props){
       super(props);
        this.state = {  
            TopDeals:[
                {
                    id:1,
                    title:"Year eBay Plus Subscription $1",
                    voted:"+342",
                    publishedDate:"20/08/2019",
                    view:329
                },
                {
                    id:2,
                    title:"Groceries",
                    voted:"+216",
                    publishedDate:"20/08/2019",
                    view:329
                },
                {
                    id:3,
                    title:"Book & Magazines",
                    voted:"+91",
                    publishedDate:"20/08/2019",
                    view:329
                },
                {
                    id:4,
                    title:"Health & Beauty",
                    voted:"+465",
                    publishedDate:"20/08/2019",
                    view:329
                },
                {
                    id:5,
                    title:"Computing",
                    voted:"+213",
                    publishedDate:"20/08/2019",
                    view:329
                },
                {
                    id:6,
                    title:"Year eBay Plus Subscription $1",
                    voted:"+342",
                    publishedDate:"20/08/2019",
                    view:329
                },
                {
                    id:7,
                    title:"Groceries",
                    voted:"+216",
                    publishedDate:"20/08/2019",
                    view:329
                },
                {
                    id:8,
                    title:"Book & Magazines",
                    voted:"+91",
                    publishedDate:"20/08/2019",
                    view:329
                },
                {
                    id:9,
                    title:"Health & Beauty",
                    voted:"+465",
                    publishedDate:"20/08/2019",
                    view:329
                },
                {
                    id:10,
                    title:"Computing",
                    voted:"+213",
                    publishedDate:"20/08/2019",
                    view:329
                }
            ]
        }
    }

    render(){
    const {classes} = this.props;
    
    return(
        <Grid
        container
        direction="column"
        justify="center"
        alignItems="stretch"
      >
        <Grid item >
          <Typography  variant="subtitle1"  className = {classes.mainTitle}>
              Front Deals
          </Typography> 
        </Grid>
        {
              this.state.TopDeals.map((category)=>{
                return(
                    <React.Fragment>
                    <Grid item key = {category.id} className = {classes.eachItem}>
                        <Grid container>
                            <Grid item xs= {12} md = {12}>
                                <Link  
                                component="button"
                                variant="body2"
                                className={classes.itemText}
                                // onClick={this.onClickViewMore} 
                                >
                                    {category.title}
                                </Link>
                            </Grid>
                            <Grid item xs = {12} md = {3}>
                                <Typography  gutterBottom  className = {classes.votedAmount} variant="subtitle2" align = "center">
                                    {category.voted}
                                </Typography> 
                            </Grid>
                            <Grid item xs = {12} md = {4}>
                                <Typography    variant="subtitle2" align = "center">
                                    {category.publishedDate}
                                </Typography>
                            </Grid>
                            <Grid item xs = {12} md = {1}>
                                <ViewIcon fontSize = "small" color = "disabled"/>
                            </Grid>
                            <Grid item xs = {12} md = {1}>
                                12
                            </Grid> 
                        </Grid>
                    </Grid>
                    <Divider/>
                    </React.Fragment>
                );
              })
          }
      </Grid>
      
    );}
}

ConnectedFrontDeals.propTypes = {
    classes: PropTypes.object.isRequired
};
const FrontDeals = connect(mapStateToProps,mapDispatchToProps)(ConnectedFrontDeals);
export default withStyles(useStyles)(FrontDeals);
