import React from 'react';
import { connect } from 'react-redux';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom';
import Divider from '@material-ui/core/Divider';
import thumb1 from '../../assets/img/1.jpg';

import TopBar from '../components/TopBar';
import SubNavBar from '../components/SubNavBar';
import DealRightPanel from '../components/DealRightPanel';

const API_URL = 'https://localhost:44380';
const useStyles = theme => ({
    root: {
        marginTop: theme.spacing(2),
    },
    eachItem: {
        '&:hover': {
            background: "#fffde7",
        },
        backgroundColor: `#ffffff`
    },
    itemText: {
        marginLeft: theme.spacing(1),
    },
    mainTitle: {
        marginTop: theme.spacing(2),
        padding: theme.spacing(1),
        fontWeight: `bold`,
        backgroundColor: `#ffe082`
    },
    subtitle: {
        fontWeight: `bold`
    },
    SubNavBarMenu: {
        cursor: 'pointer',
        color: `white`,
        fontWeight: `bold`,
        backgroundColor: `black`,
    },
    votedAmount: {
        width: theme.spacing(6),
        marginLeft: theme.spacing(2),
        borderRadius: theme.spacing(0.5),
        fontFamily: `Arial, sans-serif`,
        fontWeight: `bold`,
        background: `#388e3c`,
        color: `#ffffff`,
    },
    unVotedAmount: {
        width: theme.spacing(6),
        marginLeft: theme.spacing(2),
        borderRadius: theme.spacing(0.5),
        fontFamily: `Arial, sans-serif`,
        fontWeight: `bold`,
        background: `#ff5722`,
        color: `#ffffff`,
    },
    newBadge: {
        width: theme.spacing(6),
        marginLeft: theme.spacing(2),
        borderRadius: theme.spacing(0.5),
        fontFamily: `Arial, sans-serif`,
        fontWeight: `bold`,
        background: `#fbc02d`,
        color: `#ffffff`,
    },
    viewIcon: {
        height: theme.spacing(2),
        width: theme.spacing(2),
        color: `grey`
    },
});

function mapDispatchToProps() {
    return {

    };
}

const mapStateToProps = () => {
    return {

    };
};



class ConnectedDeals extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            dealList: []
        }
    }

    componentDidMount() {
        fetch(`${API_URL}/api/deal/getdeals`)
            .then(response => response.json())
            .then(response => this.setState({ dealList: response }))
            .catch(() => {
                alert('no register')
            })
    }
     
    render() {
        const { classes } = this.props;
        return (
            <React.Fragment>
                <TopBar />
                <div className={classes.SubNavBarMenu}>
                    <Container maxWidth="xl" >
                        <Grid container
                            direction="row"
                            justify="flex-start"
                            alignItems="flex-end"
                        >
                            <Grid item xs={12} md={1}></Grid>
                            <Grid item xs={12} md={8} >
                                <SubNavBar />
                            </Grid>
                            <Grid item xs={12} md={3}>

                            </Grid>
                        </Grid>
                    </Container>
                </div>

                <Grid
                    container
                    direction="row"
                    justify="center"
                    alignItems="stretch"
                    className={classes.root}
                >
                    <Grid ime xs={12} sm={8}>
                        <Grid item>
                            <Typography gutterBottom variant="h5" align="left" className={classes.subtitle}>
                                New Deals, Coupons, Vouchers and Freebies
                        </Typography>
                        </Grid>
                        {
                            this.state.dealList.map((deal) => {
                                return (
                                    <React.Fragment>
                                        <Grid item key={deal.dealId} className={classes.eachItem}>
                                            <Grid container
                                                direction="row"
                                                justify="flex-start"
                                                alignItems="center">
                                                <Grid item xs={12} md={1}>
                                                    <Grid container
                                                        direction="column"
                                                        justify="flex-start"
                                                        alignItems="center">
                                                        <Grid item>
                                                            <Typography gutterBottom className={classes.votedAmount} variant="subtitle2" align="center">
                                                                {deal.amountVotes} +
                                                            </Typography>
                                                        </Grid>
                                                        <Grid item>
                                                            <Typography gutterBottom className={classes.unVotedAmount} variant="subtitle2" align="center">
                                                                {deal.amountVotes} -
                                                            </Typography>
                                                        </Grid>
                                                    </Grid>
                                                </Grid>
                                                <Grid item xs={12} md={9}>
                                                    <Grid Container>
                                                        <Grid item>
                                                            <Grid
                                                                container
                                                                direction="row"
                                                                justify="flex-start"
                                                                alignItems="center">
                                                                <Grid item xs={12} md={9}>

                                                                    <Link
                                                                        component="button"
                                                                        variant="body2"
                                                                        className={classes.itemText}
                                                                        to={'/deals/' + deal.dealId}
                                                                    >
                                                                        <Typography gutterBottom variant="h6" align="left">
                                                                            {deal.title}
                                                                        </Typography>
                                                                    </Link>
                                                                </Grid>
                                                                <Grid item xs={12} md={3}>
                                                                    <Typography gutterBottom className={classes.newBadge} variant="subtitle2" align="center">
                                                                        new
                                                            </Typography>
                                                                </Grid>
                                                            </Grid>
                                                        </Grid>
                                                        <Grid item>
                                                            <Grid container
                                                                spacing={1}
                                                                direction="row"
                                                                justify="flex-start"
                                                                alignItems="center">
                                                                <Grid item>
                                                                    <Link
                                                                        component="button"
                                                                        variant="body2"
                                                                        className={classes.itemText}
                                                                    >
                                                                        <Typography gutterBottom variant="subtitle1" align="center">
                                                                            chami
                                                                        </Typography>
                                                                    </Link>
                                                                </Grid>
                                                                <Grid item>
                                                                    <Typography gutterBottom variant="subtitle1" align="center">
                                                                        {deal.startDate}
                                                                    </Typography>
                                                                </Grid>
                                                                <Grid item>
                                                                    <Link
                                                                        component="button"
                                                                        variant="body2"
                                                                        className={classes.itemText}
                                                                    >
                                                                        <Typography gutterBottom variant="subtitle2" align="center">
                                                                            {deal.linkedURL}
                                                                        </Typography>
                                                                    </Link>
                                                                </Grid>
                                                            </Grid>
                                                        </Grid>
                                                        <Grid item>
                                                            <Typography  variant="subtitle2" align="left">
                                                                {deal.descriptionText}
                                                            </Typography>
                                                        </Grid>
                                                    </Grid>
                                                </Grid>
                                                <Grid item xs={12} md={2}>
                                                    <img src={thumb1}  alt="Breakfast" />
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                        <Divider />
                                    </React.Fragment>
                                );
                            })
                        }
                    </Grid>
                    <Grid item xs={12} sm={2}>
                        <DealRightPanel />
                    </Grid>
                </Grid>
            </React.Fragment>
        );
    }
}

ConnectedDeals.propTypes = {
    classes: PropTypes.object.isRequired
};
const Deals = connect(mapStateToProps, mapDispatchToProps)(ConnectedDeals);
export default withStyles(useStyles)(Deals);
