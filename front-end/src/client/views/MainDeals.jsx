import React from 'react';
import {connect} from 'react-redux';    
import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

import Deals from './Deals';
import TopDeals from './TopDeals';
import FrontDeals from './FrontPageDeals';

const useStyles = theme => ({
    root: {
      flexGrow: 1,
      backgroundColor: theme.palette.background.paper,
    },
    topDeals:{
      width:`50%`,
      borderColor:`#ff0000`,
    },
    DealTopTitle:{
      marginLeft:theme.spacing(2),
      marginTop:theme.spacing(2),
      fontWeight:`bold`
    }
  });

  function mapDispatchToProps(dispatch) {
    return{
     
    };
  }

  const mapStateToProps = state => {
    return { 
            
    };
  };


class ConnectedMainDeals extends React.Component{
   constructor(props){
       super(props);
       this.state = {
       }
   }

    render(){
    const {classes} = this.props;
    
    return(
      <Grid container spacing={3}>
        <Grid item xs={9}>
          <Typography  variant="h5"  className = {classes.DealTopTitle}>
              New Deals, Coupons, Vouchers and Freebies
          </Typography>
            <div>
              <Deals/>
            </div> 
        </Grid>
        <Grid item xs={3} className = {classes.topDeals}>
          <TopDeals/>
          <FrontDeals/>
        </Grid>

      </Grid>
    );}
}

ConnectedMainDeals.propTypes = {
    classes: PropTypes.object.isRequired
};
const MainDeals = connect(mapStateToProps,mapDispatchToProps)(ConnectedMainDeals);
export default withStyles(useStyles)(MainDeals);
