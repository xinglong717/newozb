import React from "react";
import { connect } from "react-redux";
import Container from "@material-ui/core/Container";
import TopBar from "../components/TopBar";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Grid from "@material-ui/core/Grid";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';    
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import LiveRow from '../components/LiveRow';



const API_URL = 'https://localhost:44380';

const useStyles = theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper
  },
  headerRow:{
    backgroundColor:`#000`,
  },
  headerText:{
    color:`#fff`
  },
  liveActionCheck: {
    marginLeft: theme.spacing(2),
    marginTop: theme.spacing(1)
  },
  mainTitle: {
    marginTop: theme.spacing(2),
    marginLeft: theme.spacing(2),
    fontWeight: `bold`
  },
});

function mapDispatchToProps() {
  return {};
}

const mapStateToProps = () => {
  return {};
};

class ConnectedLive extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      checkedPosts: false,
      checkedComments: false,
      checkedVotes: false,
      checkedWiki: false,
      dealList: [],
    };
  }

    componentDidMount() {
        fetch(`${API_URL}/api/deal/getdeals`)
            .then(response => response.json())
            .then(response => this.setState({ dealList: response }))
            .catch(() => {
                alert('no register')
            })
    }

  handleChangeChecked = event => {
    this.setState({
      [event.target.id]: event.target.checked
    });
  };
  render() {
    const { classes } = this.props;

    return (
      <React.Fragment>
        <TopBar />
        <Container maxWidth="lg">
          <Typography
            gutterBottom
            variant="h5"
            align="left"
            className={classes.mainTitle}
          >
            Live Action
          </Typography>
          <Grid
            container
            direction="column"
            justify="center"
            alignItems="stretch"
          >
            <Grid item>
              <FormControlLabel
                className={classes.liveActionCheck}
                control={
                  <Checkbox
                    id="checkedPosts"
                    checked={this.state.checkedPosts}
                    onChange={this.handleChangeChecked}
                    value={this.state.checkedPosts}
                    color="primary"
                  />
                }
                label="Posts"
              />
              <FormControlLabel
                className={classes.liveActionCheck}
                control={
                  <Checkbox
                    id="checkedComments"
                    checked={this.state.checkedComments}
                    onChange={this.handleChangeChecked}
                    value={this.state.checkedComments}
                    color="primary"
                  />
                }
                label="Comments"
              />
              <FormControlLabel
                className={classes.liveActionCheck}
                control={
                  <Checkbox
                    id="checkedVotes"
                    checked={this.state.checkedVotes}
                    onChange={this.handleChangeChecked}
                    value={this.state.checkedVotes}
                    color="primary"
                  />
                }
                label="Votes"
              />
              <FormControlLabel
                className={classes.liveActionCheck}
                control={
                  <Checkbox
                    id="checkedWiki"
                    checked={this.state.checkedWiki}
                    onChange={this.handleChangeChecked}
                    value={this.state.checkedWiki}
                    color="primary"
                  />
                }
                label="Wiki"
              />
            </Grid>
           <div>
                <Paper className={classes.root} elevation={0}>
                  <Table className={classes.table}>
                    <TableHead className = {classes.headerRow}>
                      <TableRow>
                        <TableCell align="left" className = {classes.headerText}>Time</TableCell>
                        <TableCell align="left" className = {classes.headerText}>User</TableCell>
                        <TableCell align="left" className = {classes.headerText}>Action</TableCell>
                        <TableCell align="left" className = {classes.headerText}>Subject</TableCell>
                        <TableCell align="left" className = {classes.headerText}>Type</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {this.state.dealList.map((deal) => (
                        <LiveRow
                            key={deal.dealId}
                            _id={deal.dealId}
                            deal ={deal}
                        />
                      ))}
                    </TableBody>
                  </Table>
                </Paper>
                </div>
          </Grid>
        </Container>
      </React.Fragment>
    );
  }
}

ConnectedLive.propTypes = {
  classes: PropTypes.object.isRequired
};
const Live = connect(
  mapStateToProps,
  mapDispatchToProps
)(ConnectedLive);
export default withStyles(useStyles)(Live);
