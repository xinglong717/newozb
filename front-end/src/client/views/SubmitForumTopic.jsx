import React from 'react';
import Box from '@material-ui/core/Box';
import {connect} from 'react-redux';    
import Grid from '@material-ui/core/Grid';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import DealPostGuid from '../components/DealPostGuid';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import Button from '@material-ui/core/Button';

const useStyles = theme => ({
    root: {
      flexGrow: 1,
      backgroundColor: theme.palette.background.paper   ,
      margin:theme.spacing(2)
    },
    associatedCHK:{
        marginLeft:theme.spacing(2),
        marginTop:theme.spacing(1)
    },
    associatedText:{
        marginLeft:theme.spacing(4),
    },
    button:{
        margin:theme.spacing(2)
    },
    categoryCombo:{
        marginTop:theme.spacing(8),
        marginBottom:theme.spacing(4)
    },
    description:{
        width:`100%`,
        marginLeft:theme.spacing(2),
        marginTop:theme.spacing(2),
    },
    formControl:{
        marginLeft:theme.spacing(4),
        width:`30%`
    },
    nCalender:{
        marginLeft:theme.spacing(4),
        marginTop:theme.spacing(2)
    },
    tBox:{
        marginLeft:theme.spacing(1),
        width:`100%`,
        height:theme.spacing(12)
    }
  });

  function mapDispatchToProps() {
    return{
     
    };
  }

  const mapStateToProps = () => {
    return { 
            
    };
  };


class ConnectedSubmitForumTopic extends React.Component{
    constructor(props){
       super(props);
        this.state = { 
            name: '',
            // name: 'hai',
            DealCategories:[]
        }
    }

    componentDidMount() {
        fetch('https://localhost:44380/api/post/getcategories')
        .then(response =>response.json())
        .then(response =>this.setState({DealCategories:response}))
        .catch(()=>{
            alert('no register')
        })
    }

    handleChange = () => event => {
        this.setState({
          name: event.target.value,
        });
    };

    render(){
    const {classes} = this.props;
    
    return(
        <Grid
        container
        direction="row"
        justify="flex-start"
        alignItems="stretch"
        spacing = {3}
        className = {classes.root}
      >
        <Grid item xs = {12} md = {9}>
            <Grid
                container
                direction="column"
                justify="flex-start"
                alignItems="stretch"
            >
                <Grid item>
                    <Typography gutterBottom variant="h5">
                        Submit bargain
                    </Typography> 
                </Grid>
                <Grid item>
                    <TextField
                        id="outlined-full-width"
                        label="Title"
                        style={{ margin: 8 }}
                        placeholder="Placeholder"
                        helperText="Check Title Guidelines on writing concise and informative titles."
                        fullWidth
                        margin="normal"
                        variant="outlined"
                        InputLabelProps={{
                            shrink: true,
                        }}
                    />
                </Grid>
                <Grid item>
                    <TextField
                        id="outlined-full-width"
                        label="URL/Link:"
                        style={{ margin: 8 }}
                        placeholder="Placeholder"
                        helperText="Do not upload a thumbnail. An image will be generated automatically from the link you provide. Only insert your own image into the URL/Link field when no direct link to the deal exists. Click Here to submit a picture or a PDF file."
                        fullWidth
                        margin="normal"
                        variant="outlined"
                        InputLabelProps={{
                        shrink: true,
                        }}
                    />
                </Grid>
                <Grid item>
                    <TextField
                        id="outlined-full-width"
                        label="Coupon Code:"
                        style={{ margin: 8 }}
                        placeholder="Placeholder"
                        helperText="Coupon/promo code(s) associated with this bargain. Please leave this field blank if not required. If the bargain comes with multiple codes, use a space or comma to separate them. Do not use this field to add comments."
                        fullWidth
                        margin="normal"
                        variant="outlined"
                        InputLabelProps={{
                        shrink: true,
                        }}
                    />
                </Grid>
                <Grid item>
                    <Box borderColor="grey.500" border={1} className = {classes.tBox}>
                        <FormControlLabel
                            className = {classes.associatedCHK}
                            control={
                            <Checkbox
                                // checked={state.checkedB}
                                // onChange={handleChange('checkedB')}
                                value="checkedB"
                                color="primary"
                            />
                            }
                            label="I am Associated with the Store or Product"
                        />
                        <Typography gutterBottom variant="subtitle2"  className = {classes.associatedText}>
                            Tick this if you are affiliated with this store/service provider/product in any way (e.g. official store representative, employee, business partner, contractor, friend/family of staff member). IMPORTANT: Failure to declare affiliation may result in sockpuppeting penalties.
                        </Typography>
                    </Box>
                </Grid>
                <Grid item>
                    <Grid
                        container
                        direction="row"
                        justify="center"
                        alignItems="stretch"
                        spacing = {1}
                    >
                        <Grid item xs = {12} md = {6}>
                            <TextField
                                id="date"
                                label="Birthday"
                                type="date"
                                defaultValue="2017-05-24"
                                className={classes.nCalender}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                        </Grid>
                        <Grid item xs = {12} md = {6}>
                            <TextField
                                id="date"
                                label="Birthday"
                                type="date"
                                defaultValue="2017-05-24"
                                className={classes.nCalender}
                                InputLabelProps={{
                                    shrink: true,
                                }}
                            />
                        </Grid>
                    </Grid>
                    <Grid item>
                        <FormControlLabel
                            className = {classes.associatedCHK}
                            control={
                            <Checkbox
                                // checked={state.checkedB}
                                // onChange={handleChange('checkedB')}
                                value="checkedB"
                                color="primary"
                            />
                            }
                            label="I am Associated with the Store or Product"
                        />
                        <Typography gutterBottom variant="subtitle2"  className = {classes.associatedText}>
                            Tick this if you are affiliated with this store/service provider/product in any way (e.g. official store representative, employee, business partner, contractor, friend/family of staff member). IMPORTANT: Failure to declare affiliation may result in sockpuppeting penalties.
                        </Typography>
                    </Grid>
                    <Grid item>
                        <FormControl className={classes.formControl}>
                            <InputLabel htmlFor="age-native-simple">Categories</InputLabel>
                                <Select
                                    native
                                    value={this.state.age}
                                    onChange={this.handleChange('name')}
                                    className = {classes.categoryCombo}
                                    inputProps={{
                                        name: 'name',
                                        id: 'categoryCombo1',
                                    }}
                                >
                                <option value="" />
                                {
                                    this.state.DealCategories.map((category)=>{
                                        return(
                                            <option value={category.name}>{category.name}</option>
                                        );
                                    })
                                }
                            </Select>
                        </FormControl>
                    </Grid>
                    <Grid item>
                        <TextField
                            id="outlined-full-width"
                            label="Tag:"
                            style={{ margin: 8 }}
                            placeholder="Placeholder"
                            helperText="Please check Tagging Guidelines on how to properly tag a deal. Separate tags using a comma. You should use no more than 5 tags to describe this offer. Use full product names as tags, for example 'Samsung Galaxy S8'."
                            fullWidth
                            margin="normal"
                            variant="outlined"
                            InputLabelProps={{
                                shrink: true,
                            }}
                        />
                    </Grid>
                    <Grid item>
                        <TextField id="description" className = {classes.description} variant = "outlined" label = "Description" multiline = {true} rowsMax = {5}/>
                    </Grid>
                    <Grid item>
                        <Button variant="outlined" size="small" color="primary" className={classes.button}>
                            Save Draft
                        </Button>
                        <Button variant="contained" size="small" color="primary" className={classes.button}>
                            Submit
                        </Button>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
        <Grid item xs = {12} md = {3}>
            <DealPostGuid/>
        </Grid>
      </Grid>
      
    );}
}

ConnectedSubmitForumTopic.propTypes = {
    classes: PropTypes.object.isRequired
};
const SubmitForumTopic = connect(mapStateToProps,mapDispatchToProps)(ConnectedSubmitForumTopic);
export default withStyles(useStyles)(SubmitForumTopic);
