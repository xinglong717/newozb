import React from "react";
import Container from "@material-ui/core/Container";
import { connect } from "react-redux";
import Grid from "@material-ui/core/Grid";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import DealPostGuid from "../components/DealPostGuid";
import { Link } from "react-router-dom";
import TopBar from "../components/TopBar";
import SubNavBar from "../components/SubNavBar";
import SubToolBar from "../components/SubToolBar";
import thumb1 from "../../assets/img/1.jpg";

const API_URL = "https://localhost:44380";
const useStyles = theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    margin: theme.spacing(2)
  },
  SubNavBarMenu: {
    cursor: "pointer",
    color: `white`,
    fontWeight: `bold`,
    backgroundColor: `black`
  },
  thumbImg: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1)
  },
  tBox: {
    marginLeft: theme.spacing(1),
    width: `100%`,
    height: theme.spacing(16)
  },
  title: {
    marginLeft: theme.spacing(2)
  },
  votedAmount: {
    width: theme.spacing(6),
    marginLeft: theme.spacing(2),
    borderRadius: theme.spacing(0.5),
    fontFamily: `Arial, sans-serif`,
    fontWeight: `bold`,
    background: `#388e3c`,
    color: `#ffffff`
  },
  unVotedAmount: {
    width: theme.spacing(6),
    marginLeft: theme.spacing(2),
    borderRadius: theme.spacing(0.5),
    fontFamily: `Arial, sans-serif`,
    fontWeight: `bold`,
    background: `#ff5722`,
    color: `#ffffff`
  },
  newBadge: {
    width: theme.spacing(6),
    marginLeft: theme.spacing(2),
    borderRadius: theme.spacing(0.5),
    fontFamily: `Arial, sans-serif`,
    fontWeight: `bold`,
    background: `#fbc02d`,
    color: `#ffffff`
  }
});

function mapDispatchToProps() {
  return {};
}

const mapStateToProps = () => {
  return {};
};

class ConnectedDetailDeal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      deal: []
    };
  }

  componentDidMount() {
    fetch(
      `${API_URL}/api/deal/getdeal?dealId=` + this.props.match.params.dealId,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json"
        }
      }
    )
      .then(response => response.json())
      .then(response =>
        this.setState({
          deal: response
        })
      );
  }

  render() {
    const { classes } = this.props;

    return (
      <React.Fragment>
        <TopBar />
        <div className={classes.SubNavBarMenu}>
          <Container maxWidth="xl">
            <Grid
              container
              direction="row"
              justify="flex-start"
              alignItems="flex-end"
            >
              <Grid item xs={12} md={1}></Grid>
              <Grid item xs={12} md={8}>
                <SubNavBar />
              </Grid>
            </Grid>
          </Container>
        </div>
        <Container maxWidth="lg">
          <Grid
            container
            direction="row"
            justify="flex-start"
            alignItems="stretch"
            spacing={3}
            className={classes.root}
          >
            <Grid item xs={12} md={9}>
              <Grid
                container
                direction="column"
                justify="flex-start"
                alignItems="stretch"
              >
                <Grid item>
                  <Typography
                    gutterBottom
                    variant="h5"
                    className={classes.title}
                  >
                    {this.state.deal.title}
                  </Typography>
                </Grid>
                <Grid item>
                  <SubToolBar />
                </Grid>
                <Grid item>
                  <Grid item className={classes.eachItem}>
                    <Grid
                      container
                      direction="row"
                      justify="flex-start"
                      alignItems="center"
                    >
                      <Grid item xs={12} md={1}>
                        <Grid
                          container
                          direction="column"
                          justify="flex-start"
                          alignItems="center"
                        >
                          <Grid item>
                            <Typography
                              gutterBottom
                              className={classes.votedAmount}
                              variant="subtitle2"
                              align="center"
                            >
                              {this.state.deal.amountVotes} +
                            </Typography>
                          </Grid>
                          <Grid item>
                            <Typography
                              gutterBottom
                              className={classes.unVotedAmount}
                              variant="subtitle2"
                              align="center"
                            >
                              {this.state.deal.amountVotes} -
                            </Typography>
                          </Grid>
                        </Grid>
                      </Grid>
                      <Grid item xs={12} md={9}>
                        <Grid Container>
                          <Grid item>
                            <Grid
                              container
                              spacing={1}
                              direction="row"
                              justify="flex-start"
                              alignItems="center"
                            >
                              <Grid item>
                                <Link
                                  component="button"
                                  variant="body2"
                                  className={classes.itemText}
                                >
                                  <Typography
                                    gutterBottom
                                    variant="subtitle1"
                                    align="center"
                                  >
                                    chami
                                  </Typography>
                                </Link>
                              </Grid>
                              <Grid item>
                                <Typography
                                  gutterBottom
                                  variant="subtitle1"
                                  align="center"
                                >
                                  {this.state.deal.startDate}
                                </Typography>
                              </Grid>
                              <Grid item>
                                <Link
                                  component="button"
                                  variant="body2"
                                  className={classes.itemText}
                                >
                                  <Typography
                                    gutterBottom
                                    variant="subtitle2"
                                    align="center"
                                  >
                                    {this.state.deal.linkedURL}
                                  </Typography>
                                </Link>
                              </Grid>
                            </Grid>
                          </Grid>
                          <Grid item>
                            <Typography
                              gutterBottom
                              variant="subtitle2"
                              align="left"
                            >
                              {this.state.deal.descriptionText}
                            </Typography>
                          </Grid>
                        </Grid>
                      </Grid>
                      <Grid item xs={12} md={2}>
                        <img
                          src={thumb1}
                          className={classes.thumbImg}
                          alt="Breakfast"
                        />
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={12} md={3}>
              <DealPostGuid />
            </Grid>
          </Grid>
        </Container>
      </React.Fragment>
    );
  }
}

ConnectedDetailDeal.propTypes = {
  classes: PropTypes.object.isRequired
};
const DetailDeal = connect(
  mapStateToProps,
  mapDispatchToProps
)(ConnectedDetailDeal);
export default withStyles(useStyles)(DetailDeal);
