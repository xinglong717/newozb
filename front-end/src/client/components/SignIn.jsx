import React from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';



import {closeSignInDialog} from '../../js/actions/Home';
import {isUserSigned} from '../../js/actions/Home';

function MadeWithLove() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Built with love by the '}
      <Link color="inherit" href="https://material-ui.com/">
        Chami-Soft
      </Link>
      {' team.'}
    </Typography>
  );
}

const API_URL = 'https://localhost:44380';

const useStyles = theme => ({
  '@global': {
    body: {
      backgroundColor: theme.palette.common.white,
    },
  },
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
});



function mapDispatchToProps(dispatch){
  return{
    gotoCloseSignIn:() =>{ dispatch(closeSignInDialog())},
    gotoIsUserSigned:() =>{ dispatch(isUserSigned())},
  };
}




class ConnectedSignIn extends React.Component{

  constructor(props){
    super(props);
    this.state = {
      email: "",
      password: ""
    };
  }

  validateForm(){
    return this.state.email.length > 0 && this.state.password.length > 0;
    // console.log(this.state.email.value);
  }

  handleChange = event => {
    this.setState({
      [event.target.id]:event.target.value
    });
  }

/**
 * When click SingIn button,close Dialog
 */
  closeSignInDialogHandle = ()=>{
    this.props.gotoCloseSignIn();
  }

  handleSubmit = event => {
    event.preventDefault();
    const email = this.state.email;
    const password = this.state.password;
    fetch(`${API_URL}/api/Auth`, {
      method: 'POST', // or 'PUT'
      body: JSON.stringify({ email, password }), // data can be `string` or {object}!
      headers:{
        'Content-Type': 'application/json'
      }
    })
    .then(response =>response.json())
    .then(response=>{
        localStorage.setItem('userInfo',response.firstName+response.lastName)
        localStorage.setItem('usertoken', response.token)
    })
    .then(this.closeSignInDialogHandle)
    .then(this.props.gotoIsUserSigned)
    .catch(error=>{
        alert('no register')
    })
  }


  render(){
    const {classes} = this.props;
    return (
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          <form className={classes.form} onSubmit = {this.handleSubmit}>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email Address"
              name="email"
              autoComplete="email"
              value = {this.state.email}
              onChange = {this.handleChange}
              autoFocus
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              value = {this.state.password}
              onChange = {this.handleChange}
              autoComplete="current-password"
            />
            <FormControlLabel
              control={<Checkbox value="remember" color="primary" />}
              label="Remember me"
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              disabled={!this.validateForm()}
              className={classes.submit}
            >
              Sign In
            </Button>
            <Grid container>
              <Grid item xs>
                <Link href="#" variant="body2">
                  Forgot password?
                </Link>
              </Grid>
              <Grid item>
                <Link href="#" variant="body2">
                  {"Don't have an account? Sign Up"}
                </Link>
              </Grid>
            </Grid>
          </form>
        </div>
        <Box mt={5}>
          <MadeWithLove />
        </Box>
      </Container>
    );
  }
}

ConnectedSignIn.propTypes = {
  classes: PropTypes.object.isRequired
};

const SignIn = connect(null,mapDispatchToProps)(ConnectedSignIn);
export default withStyles(useStyles)(SignIn);
