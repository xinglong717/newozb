import React from "react";
import { connect } from "react-redux";
import Toolbar from "@material-ui/core/Toolbar";
import { Link } from "react-router-dom";
import Container from "@material-ui/core/Container";
import Grid from "@material-ui/core/Grid";
import PropTypes from "prop-types";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import { withStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import SubMenuForum from "./SubMenuForum";
import SubMenuDeal from "./SubMenuDeal";

import ShowSignPart from './TopBar/SignPart';
import AccountPage from './TopBar/Account';


import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from "reactstrap";
import "bootstrap/dist/css/bootstrap.css";

const useStyles = theme => ({
  root: {
    flexGrow: 1
  },
  mainMenuText: {
    fontWeight: `bold`,
    color: `#000`,
    flexDirection: "column",
    justifyContent: "center",
    "&:hover": {
      color: "#fff"
    }
  },
  markImage: {
    cursor: "pointer",
    fontWeight: `bold`,
    color: `#fff`
  },
  navItem: {
    borderRadius: 0,
    border: 0,
    cursor: "pointer",
    "&:hover": {
      background: "#6d4c41",
      color: `#fff`
    },
    "&:after": {
      display: `none`
    },
    backgroundColor: `#f57f17`
  },
  toolbar: {
    flexGrow: 1,
    minHeight: theme.spacing(6),
    borderBottom: `1px solid ${theme.palette.divider}`
  }
});

const mapStateToProps = state => {
  return {
    isUserSigned: state.Home.isUserSigned,
  };
};
function mapDispatchToProps(dispatch) {
  return {};
}

class ConnectedTopBar extends React.Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.onMouseEnter = this.onMouseEnter.bind(this);
    this.onMouseLeave = this.onMouseLeave.bind(this);
    this.state = {
      dropdownOpenSubDealMenu: false,
      dropdownOpenSubForumMenu: false
    };
  }

  toggle(target) {
    if (target === "Deal") {
      this.setState(prevState => ({
        dropdownOpenSubDealMenu: !prevState.dropdownOpenSubDealMenu
      }));
    } else if (target === "Forum") {
      this.setState(prevState => ({
        dropdownOpenSubForumMenu: !prevState.dropdownOpenSubForumMenu
      }));
    }
  }

  onMouseEnter(target) {
    if (target === "Deal") {
      this.setState({ dropdownOpenSubDealMenu: true });
    } else if (target === "Forum") {
      this.setState({ dropdownOpenSubForumMenu: true });
    }
  }

  onMouseLeave(target) {
    if (target === "Deal") {
      this.setState({ dropdownOpenSubDealMenu: false });
    } else if (target === "Forum") {
      this.setState({ dropdownOpenSubForumMenu: false });
    }
  }

  render() {
    const { classes } = this.props;

    return (
      <React.Fragment>
        <Toolbar className={classes.toolbar} style={{ background: "#f57f17" }}>
          <Container maxWidth="xl">
            <Grid
              container
              direction="row"
              justify="flex-start"
              alignItems="flex-end"
            >
              <Grid item xs={12} md={2}>
                <Link to={"/"} style={{ textDecoration: "none" }}>
                  <Typography
                    gutterBottom
                    variant="h5"
                    className={classes.markImage}
                  >
                    OzBargain
                  </Typography>
                </Link>
              </Grid>
              <Grid item xs={12} md={7}>
                <Grid
                  container
                  direction="row"
                  justify="flex-start"
                  alignItems="center"
                >
                  <Grid item>
                    <Dropdown
                      onMouseOver={e => this.onMouseEnter("Deal", e)}
                      onMouseLeave={e => this.onMouseLeave("Deal", e)}
                      isOpen={this.state.dropdownOpenSubDealMenu}
                      toggle={e => this.toggle("Deal", e)}
                    >
                      <Link to={"/deals"} style={{ textDecoration: "none" }}>
                        <DropdownToggle caret className={classes.navItem}>
                          <Typography
                            variant="h6"
                            className={classes.mainMenuText}
                          >
                            Deals
                          </Typography>
                        </DropdownToggle>
                      </Link>
                      <DropdownMenu>
                        <DropdownItem header>
                          <SubMenuDeal />
                        </DropdownItem>
                      </DropdownMenu>
                    </Dropdown>
                  </Grid>
                  <Grid item>
                    <Link to={"/Live"} style={{ textDecoration: "none" }}>
                      <Box alignItems="center">
                        <Box p={1} className={classes.navItem}>
                          <Typography
                            variant="h6"
                            className={classes.mainMenuText}
                          >
                            Live
                          </Typography>
                        </Box>
                      </Box>
                    </Link>
                  </Grid>
                  <Grid item>
                    <Dropdown
                      onMouseOver={e => this.onMouseEnter("Forum", e)}
                      onMouseLeave={e => this.onMouseLeave("Forum", e)}
                      isOpen={this.state.dropdownOpenSubForumMenu}
                      toggle={e => this.toggle("Forum", e)}
                    >
                      <Link to={"/forums"} style={{ textDecoration: "none" }}>
                        <DropdownToggle caret className={classes.navItem}>
                          <Typography
                            variant="h6"
                            className={classes.mainMenuText}
                          >
                            Forums
                          </Typography>
                        </DropdownToggle>
                      </Link>
                      <DropdownMenu>
                        <DropdownItem header>
                          <SubMenuForum />
                        </DropdownItem>
                      </DropdownMenu>
                    </Dropdown>
                  </Grid>

                  <Grid item>
                    {this.props.isUserSigned ? <AccountPage/> : <ShowSignPart />}
                  </Grid>
                </Grid>
              </Grid>
              <Grid item xs={12} md={3}></Grid>
            </Grid>
          </Container>
        </Toolbar>
        <CssBaseline />
      </React.Fragment>
    );
  }
}

ConnectedTopBar.propTypes = {
  classes: PropTypes.object.isRequired
};
const TopBar = connect(
  mapStateToProps,
  mapDispatchToProps
)(ConnectedTopBar);
export default withStyles(useStyles)(TopBar);
