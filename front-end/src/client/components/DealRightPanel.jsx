import React from 'react';
import { connect } from 'react-redux';
import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';





const useStyles = theme => ({
  root: {
    flexGrow: 1,
    marginLeft: theme.spacing(2)
  },
  associatedText: {
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2)
  },
  eachItem: {
    backgroundColor: `#fffde7`
  },
  itemText: {
    marginLeft: theme.spacing(1),
  },
  mainTitle: {
    padding: theme.spacing(1),
    fontWeight: `bold`,
    backgroundColor: `#ffe082`
  },
  subTitle: {
    fontWeight: `bold`,
    marginLeft: theme.spacing(2)
  },
  votedAmount: {
    width: theme.spacing(6),
    marginLeft: theme.spacing(2),
    borderRadius: theme.spacing(0.5),
    fontFamily: `Arial, sans-serif`,
    fontWeight: `bold`,
    background: `#388e3c`,
    color: `#ffffff`,
  },
  viewIcon: {
    height: theme.spacing(2),
    width: theme.spacing(2),
    color: `grey`
  },
  tBox: {
    margin: theme.spacing(1),

  }
});

function mapDispatchToProps() {
  return {

  };
}

const mapStateToProps = () => {
  return {

  };
};


class ConnectedDealRightPanel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  }

  render() {
    const { classes } = this.props;

    return (
      <Grid
        container
        direction="column"
        justify="center"
        alignItems="stretch"
        className={classes.root}
      >
        <Grid item >
          <Typography variant="subtitle1" className={classes.mainTitle}>
            Deal Posting Guidelines
          </Typography>
        </Grid>
        <Grid item className={classes.eachItem}>
          <Box borderColor="error.main" border={1} className={classes.tBox}>
            <Typography gutterBottom variant="subtitle1" className={classes.subTitle}>DUPLICATES</Typography>
            <Typography gutterBottom variant="subtitle2" className={classes.associatedText}>
              Check to see if the deal has been posted by:
              Browsing through the store page. (e.g. Gearbest)
              Searching for the product by typing the make then model until the product shows up in the drop down (e.g. Nintendo Switch).
              Found a duplicate? Has that deal expired and been posted less than 30 days ago? Report the deal and we'll update it.
                </Typography>
          </Box>
          <Typography gutterBottom variant="subtitle1" className={classes.subTitle}>5 Tips to Good Deals</Typography>
          <Typography gutterBottom variant="subtitle2" className={classes.associatedText}>
            Titles should always include the name of the product or service, price, and the name of the store. (e.g. Acme Anvil $49 (Was $99) @ Acme Corp)
            Link directly to the deal (even if it's Facebook) and only upload a picture if there is no link at all.
            Make sure there is enough quantity (at least 10 in stock).
            Work for the company offering the deal? Is the owner a friend or family member? Is someone paying you to post? Yes? Then click *I am Associated with the Store*
            Referral codes and links are not permitted anywhere in the deal.
            </Typography>
        </Grid>
        <Grid item>
        </Grid>
      </Grid>

    );
  }
}

ConnectedDealRightPanel.propTypes = {
  classes: PropTypes.object.isRequired
};
const DealRightPanel = connect(mapStateToProps, mapDispatchToProps)(ConnectedDealRightPanel);
export default withStyles(useStyles)(DealRightPanel);
