import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import { Button, Divider } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import { Link } from "react-router-dom";

const useStyles = theme => ({
  root: {
    flexGrow: 1,
    width: theme.spacing(42),
    height: theme.spacing(56),
    cursor: "pointer"
  },
  categoryBar: {
    backgroundColor: `#e0e0e0`
  },
  eachItem: {
    height: theme.spacing(4),
    "&:hover": {
      background: "#ffecb3"
    }
  },
  submitButton: {
    marginLeft: theme.spacing(1),
    marginTop: theme.spacing(0.5),
    marginBottom: theme.spacing(1)
  },
  categoryText: {
    marginLeft: theme.spacing(1),
    fontWeight: `bold`
  },
  itemText: {
    marginLeft: theme.spacing(1)
  }
});

const mapStateToProps = () => {
  return {};
};

function mapDispatchToProps() {
  return {
  };
}

class ConnectedSubMenuDeal extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      mainDealFuncs: [
        {
          id: 1,
          itemText: "New Deals"
        },
        {
          id: 2,
          itemText: "Deals Expiring Soon"
        },
        {
          id: 3,
          itemText: "Freebies"
        },
        {
          id: 4,
          itemText: "Deals Starting Soon"
        },
        {
          id: 5,
          itemText: "Stores"
        },
        {
          id: 6,
          itemText: "Long Running Deals"
        },
        {
          id: 7,
          itemText: "Popular Deals"
        }
      ],
      DealCategories: []
    };
  }

  componentDidMount() {
    fetch("https://localhost:44380/api/post/getcategories")
      .then(response => response.json())
      .then(response => this.setState({ DealCategories: response }))
      .catch(() => {
        alert("no register");
      });
  }

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Grid
          container
          spacing={1}
          direction="row"
          justify="flex-start"
          alignItems="center"
        >
          <Grid item xs={12}>
            <Link to={"/deal/submit"} style={{ textDecoration: "none" }}>
              <Button
                variant="outlined"
                size="small"
                className={classes.submitButton}
              >
                +Submit Deal
              </Button>
            </Link>
            <Divider />
          </Grid>
          {this.state.mainDealFuncs.map(category => {
            return (
              <Grid
                item
                xs={12}
                sm={6}
                key={category.id}
                className={classes.eachItem}
              >
                <Link
                  component="button"
                  variant="body2"
                  style={{ textDecoration: "none" }}
                  to={"/category/" + 1}
                >
                  <Typography variant="subtitle2" className={classes.itemText}>
                    {category.itemText}
                  </Typography>
                </Link>
              </Grid>
            );
          })}
          <Grid item xs={12} sm={12} className={classes.categoryBar}>
            <Typography variant="subtitle2" className={classes.categoryText}>
              Categories
            </Typography>
          </Grid>
          {this.state.DealCategories.map(category => {
            return (
              <Grid
                item
                xs={12}
                sm={6}
                key={category.id}
                className={classes.eachItem}
              >
                <Link
                  component="button"
                  variant="body2"
                  style={{ textDecoration: "none" }}
                  to={"/category/" + 1}
                >
                  <Typography variant="subtitle2" className={classes.itemText}>
                    {category.name}
                  </Typography>
                </Link>
              </Grid>
            );
          })}
        </Grid>
      </div>
    );
  }
}

ConnectedSubMenuDeal.propTypes = {
  classes: PropTypes.object.isRequired
};

const SubMenuDeal = connect(
  mapStateToProps,
  mapDispatchToProps
)(ConnectedSubMenuDeal);

export default withStyles(useStyles)(SubMenuDeal);
