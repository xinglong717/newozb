import React from "react";
import { connect } from "react-redux";
import Grid from "@material-ui/core/Grid";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";

const useStyles = theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: `#fffde7`,
    marginLeft: theme.spacing(2)
  },
  itemText: {
    cursor: "pointer",
    paddingLeft: theme.spacing(1),
    paddingRight: theme.spacing(1),
    "&:hover": {
      background: "#ffa000",
      color: `#fff`
    }
  }
});

function mapDispatchToProps() {
  return {};
}

const mapStateToProps = () => {
  return {};
};

class ConnectedSubToolBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { classes } = this.props;

    return (
      <Grid
        container
        direction="row"
        justify="flex-start"
        alignItems="stretch"
        className={classes.root}
      >
        <Grid item>
          <Typography variant="subtitle1" className={classes.itemText}>
            view
          </Typography>
        </Grid>
        <Grid item>
          <Typography variant="subtitle1" className={classes.itemText}>
            revisions
          </Typography>
        </Grid>
        <Grid item>
          <Typography variant="subtitle1" className={classes.itemText}>
            votes
          </Typography>
        </Grid>
      </Grid>
    );
  }
}

ConnectedSubToolBar.propTypes = {
  classes: PropTypes.object.isRequired
};
const SubToolBar = connect(
  mapStateToProps,
  mapDispatchToProps
)(ConnectedSubToolBar);
export default withStyles(useStyles)(SubToolBar);
