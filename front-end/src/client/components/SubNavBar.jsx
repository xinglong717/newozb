import React from 'react';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';



const useStyles = theme => ({
    root: {
      flexGrow: 1,
    },
    subTitle:{
        '&:hover': {
            background: "#6d4c41",
            color:"#111"
        }
    },
    itemText:{
      marginRight:theme.spacing(2)
    }
  });

class SubNavBar  extends React.Component{
    constructor(props){
      super(props);
      this.state ={
        subMenus:[
          {
              id:1,
              itemText:"Electrical & Electronics"
          },
          {
              id:2,
              itemText:"Travel"
          },
          {
              id:3,
              itemText:"Fashion & Apparel"
          },
          {
              id:4,
              itemText:"Groceries"
          },
          {
              id:5,
              itemText:"Home & Garden"
          },
          {
              id:6,
              itemText:"Gaming"
          },
          {
              id:7,
              itemText:"Computing"
          },
          {
              id:8,
              itemText:"Mobile"
          },
        ],
      }
    }
    render(){
        const classes = this.props;
        return(
            <Container maxWidth = "xl">
                <Grid container spacing={4}
                  direction="row"
                  justify="flex-start"
                  alignItems="flex-end">
                  {
                    this.state.subMenus.map((category)=>{
                      return(
                          <Grid item key = {category.id} className = {classes.subTitle}>
                              <Typography  variant="subtitle1"  className = {classes.itemText}>
                                  {category.itemText}
                              </Typography>
                          </Grid>
                      );
                    })
                }
                </Grid>
            </Container>
        );
    }
}
SubNavBar.propTypes = {
    classes: PropTypes.object.isRequired
  };
export default withStyles(useStyles)(SubNavBar);