import React from 'react';
import { connect } from "react-redux";
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import { Divider } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';


const useStyles = theme => ({
  root: {
    flexGrow: 1,
    width:theme.spacing(48),
    height:theme.spacing(58),
    cursor: 'pointer',
  },
  categoryBar:{
    backgroundColor:`#e0e0e0`
  },
  eachItem:{
    '&:hover': {
      background: "#ffecb3",
    }
  },
  submitButton:{
      marginLeft:theme.spacing(1),
      marginTop:theme.spacing(0.5),
      marginBottom:theme.spacing(1)
  },
  categoryText:{
    marginLeft:theme.spacing(1),
    fontWeight:`bold`
  },
  itemText:{
    marginLeft:theme.spacing(1),
  }
});


const mapStateToProps = state => {
  return { 
      
  };
};

  function mapDispatchToProps(dispatch) {
    return{
     
    };
  }


class ConnectedSubMenuForum extends React.Component{
  constructor(props){
      super(props);
      
      this.state = {
          mainDealFuncs:[
                {
                    id:1,
                    itemText:"New Deals"
                },
                {
                    id:2,
                    itemText:"Deals Expiring Soon"
                },
                {
                    id:3,
                    itemText:"Freebies"
                },
                {
                    id:4,
                    itemText:"Deals Starting Soon"
                },
                {
                    id:5,
                    itemText:"Stores"
                },
                {
                    id:6,
                    itemText:"Long Running Deals"
                },
                {
                    id:7,
                    itemText:"Popular Deals"
                },
          ],
        DealCategories:[],
        MainCategory:[
            {
                id:1,
                name:"All Forums"
            },
            {
                id:2,
                name:"Polls"
            },
            {
                id:3,
                name:"New Discussions"
            },
            {
                id:4,
                name:"Unanswered"
            },
            {
                id:5,
                name:"Hot Discussions"
            },
        ],
        eCategory:[
            {
                id:1,
                name:"General Discussion"
            },
            {
                id:2,
                name:"Free Software/Websites"
            },
            {
                id:3,
                name:"Group Buy Deals"
            },
            {
                id:4,
                name:"Find Me Bargain"
            }
        ],
        ozbCategories:[
            {
                id:1,
                name:"Site Discussion"
            },
            {
                id:2,
                name:"Introduce Yourself"
            }
        ]
      }
  }

  componentDidMount() {
    fetch('https://localhost:44380/api/post/getcategories')
    .then(response =>response.json())
    .then(response =>this.setState({DealCategories:response}))
    .catch(error=>{
        alert('no register')
    })
  }

  render(){
    const {classes} = this.props;
    return(
        <div className={classes.root}>
        <Grid container spacing={1} direction="row"
            justify="flex-start"
            alignItems="center">
          {
              this.state.MainCategory.map((category)=>{
                return(
                    <Grid item xs={12} sm={6} key = {category.id} className = {classes.eachItem}>
                        <Typography  variant="subtitle2"  className = {classes.itemText}>
                            {category.name}
                        </Typography>
                    </Grid>
                );
              })
          }
            <Divider/>
            <Grid item xs = {12} sm = {12} className = {classes.categoryBar}>
                <Typography  variant="subtitle2"  className = {classes.categoryText}>Categories</Typography>
            </Grid>
          {
              this.state.DealCategories.map((category)=>{
                return(
                    <Grid item xs={12} sm={6} key = {category.id} className = {classes.eachItem}>
                        <Typography  variant="subtitle2"  className = {classes.itemText}>
                            {category.name}
                        </Typography>
                    </Grid>
                );
              })
          }
            <Grid item xs = {12} sm = {12} className = {classes.categoryBar}>
                <Typography  variant="subtitle2"  className = {classes.categoryText}>Bargains,Deals and Freebies</Typography>
            </Grid>
          {
              this.state.eCategory.map((category)=>{
                return(
                    <Grid item xs={12} sm={6} key = {category.id} className = {classes.eachItem}>
                        <Typography  variant="subtitle2"  className = {classes.itemText}>
                            {category.name}
                        </Typography>
                    </Grid>
                );
              })
          }
            <Grid item xs = {12} sm = {12} className = {classes.categoryBar}>
                <Typography  variant="subtitle2"  className = {classes.categoryText}>OzBargain</Typography>
            </Grid>
          {
              this.state.ozbCategories.map((category)=>{
                return(
                    <Grid item xs={12} sm={6} key = {category.id} className = {classes.eachItem}>
                        <Typography  variant="subtitle2"  className = {classes.itemText}>
                            {category.name}
                        </Typography>
                    </Grid>
                );
              })
          }
        </Grid>
      </div>
     
  );}
}

ConnectedSubMenuForum.propTypes = {
  classes: PropTypes.object.isRequired
};

const SubMenuForum = connect(mapStateToProps,mapDispatchToProps)(ConnectedSubMenuForum);

export default withStyles(useStyles)(SubMenuForum);