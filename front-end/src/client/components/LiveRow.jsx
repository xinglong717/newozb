import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import TableCell from "@material-ui/core/TableCell";
import TableRow from "@material-ui/core/TableRow";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

const useStyles = theme => ({});

class ConnectedLiveRow extends React.Component {
  render() {
    return (
      <TableRow>
        <TableCell> {this.props.deal.dealId}</TableCell>
        <TableCell align="left">{this.props.deal.dealId}</TableCell>
        <TableCell align="left">{this.props.deal.dealId}</TableCell>
        <TableCell align="left">
          <Link
            component="button"
            variant="subtitle1"
            to={"/deals/" + this.props.deal.dealId}
          >
            {this.props.deal.title}
          </Link>
        </TableCell>
        <TableCell align="left">{this.props.deal.dealId}</TableCell>
      </TableRow>
    );
  }
}

ConnectedLiveRow.propTypes = {
  classes: PropTypes.object.isRequired
};
const LiveRow = connect(
  null,
  null
)(ConnectedLiveRow);
export default withStyles(useStyles)(LiveRow);
