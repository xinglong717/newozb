import React from 'react';
import { connect } from "react-redux";
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';


const useStyles = theme => ({
  root: {
    flexGrow: 1,
    width:theme.spacing(24),
    height:theme.spacing(36),
    cursor: 'pointer',
  },
  categoryBar:{
    backgroundColor:`#e0e0e0`
  },
  eachItem:{
    '&:hover': {
      background: "#ffecb3",
    }
  },
  submitButton:{
      marginLeft:theme.spacing(1),
      marginTop:theme.spacing(0.5),
      marginBottom:theme.spacing(1)
  },
  categoryText:{
    marginLeft:theme.spacing(1),
    fontWeight:`bold`
  },
  itemText:{
    marginLeft:theme.spacing(1),
  }
});


const mapStateToProps = state => {
  return { 
      
  };
};

  function mapDispatchToProps(dispatch) {
    return{
     
    };
  }


class ConnectedSubMenuPage extends React.Component{
  constructor(props){
      super(props);
      
      this.state = {
        categories:[
            {
                id:1,
                name:"Fathers Day 2019Deals"
            },
            {
                id:2,
                name:"Birthday Deals"
            },
            {
                id:3,
                name:"Hotel Deals"
            },
            {
                id:4,
                name:"Mobile Plan Deals"
            },
            {
                id:5,
                name:"Pizza Coupons"
            },
            {
                id:6,
                name:"What Should I Buy"
            },
            {
                id:7,
                name:"Wiki,The BargainPedia"
            },
        ]
      }
  }

//   componentDidMount() {
//     fetch('https://localhost:44380/api/post/getcategories')
//     .then(response =>response.json())
//     .then(response =>this.setState({DealCategories:response}))
//     .catch(error=>{
//         alert('no register')
//     })
//   }

  render(){
    const {classes} = this.props;
    return(
        <div className={classes.root}>
        <Grid container spacing={1} direction="row"
            justify="flex-start"
            alignItems="center">
          {
              this.state.categories.map((category)=>{
                return(
                    <Grid item xs={12}  key = {category.id} className = {classes.eachItem}>
                        <Typography  variant="subtitle2"  className = {classes.itemText}>
                            {category.name}
                        </Typography>
                    </Grid>
                );
              })
          }
        </Grid>
      </div>
     
  );}
}

ConnectedSubMenuPage.propTypes = {
  classes: PropTypes.object.isRequired
};

const SubMenuPage = connect(mapStateToProps,mapDispatchToProps)(ConnectedSubMenuPage);

export default withStyles(useStyles)(SubMenuPage);