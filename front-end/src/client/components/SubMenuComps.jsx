import React from 'react';
import { connect } from "react-redux";
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import { Button, Divider } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';


const useStyles = theme => ({
  root: {
    flexGrow: 1,
    width: theme.spacing(42),
    height: theme.spacing(58),
    cursor: 'pointer',
  },
  categoryBar: {
    backgroundColor: `#e0e0e0`
  },
  eachItem: {
    '&:hover': {
      background: "#ffecb3",
    }
  },
  submitButton: {
    marginLeft: theme.spacing(1),
    marginTop: theme.spacing(0.5),
    marginBottom: theme.spacing(1)
  },
  categoryText: {
    marginLeft: theme.spacing(1),
    fontWeight: `bold`
  },
  name: {
    marginLeft: theme.spacing(1),
  }
});


const mapStateToProps = state => {
  return {

  };
};

function mapDispatchToProps(dispatch) {
  return {

  };
}


class ConnectedSubMenuComps extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      mainDealFuncs: [
        {
          id: 1,
          name: "All Competitions"
        },
        {
          id: 2,
          name: "Recently Closed"
        },
        {
          id: 3,
          name: "Popular Competitions"
        },
        {
          id: 4,
          name: "Winners"
        },
        {
          id: 5,
          name: "Closing Soon"
        }
      ],
      types: [
        {
          id: 1,
          name: "Creative/Design"
        },
        {
          id: 2,
          name: "No of words or less"
        },
        {
          id: 3,
          name: "Guessing Other Skills"
        },
        {
          id: 4,
          name: "Instant Win Daily Entry"
        },
        {
          id: 5,
          name: "Lucky Draw"
        }
      ],
      openTo: [
        {
          id: 1,
          name: "AOT"
        },
        {
          id: 2,
          name: "SA"
        },
        {
          id: 3,
          name: "NSW"
        },
        {
          id: 4,
          name: "TAS"
        },
        {
          id: 5,
          name: "NT"
        },
        {
          id: 6,
          name: "VIC"
        },
        {
          id: 7,
          name: "VIC"
        },
        {
          id: 8,
          name: "QLD"
        },
        {
          id: 9,
          name: "WA"
        }
      ],
      DealCategories: []
    }
  }

  //   componentDidMount() {
  //     fetch('https://localhost:44380/api/post/getcategories')
  //     .then(response =>response.json())
  //     .then(response =>this.setState({DealCategories:response}))
  //     .catch(error=>{
  //         alert('no register')
  //     })
  //   }

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Grid container spacing={1} direction="row"
          justify="flex-start"
          alignItems="center">
          <Grid item xs={12}>
            <Button variant="outlined" size="small" className={classes.submitButton}>
              +Submit Competition
            </Button>
            <Divider />
          </Grid>
          {
            this.state.mainDealFuncs.map((category) => {
              return (
                <Grid item xs={12} sm={6} key={category.id} className={classes.eachItem}>
                  <Typography variant="subtitle2" className={classes.name}>
                    {category.name}
                  </Typography>
                </Grid>
              );
            })
          }
          <Grid item xs={12} sm={12} className={classes.categoryBar}>
            <Typography variant="subtitle2" className={classes.categoryText}>Types</Typography>
          </Grid>
          {
            this.state.types.map((category) => {
              return (
                <Grid item xs={12} sm={6} key={category.id} className={classes.eachItem}>
                  <Typography variant="subtitle2" className={classes.name}>
                    {category.name}
                  </Typography>
                </Grid>
              );
            })
          }
          <Grid item xs={12} sm={12} className={classes.categoryBar}>
            <Typography variant="subtitle2" className={classes.categoryText}>Open To</Typography>
          </Grid>
          {
            this.state.openTo.map((category) => {
              return (
                <Grid item xs={12} sm={6} key={category.id} className={classes.eachItem}>
                  <Typography variant="subtitle2" className={classes.name}>
                    {category.name}
                  </Typography>
                </Grid>
              );
            })
          }
        </Grid>
      </div>
    );
  }
}

ConnectedSubMenuComps.propTypes = {
  classes: PropTypes.object.isRequired
};

const SubMenuComps = connect(mapStateToProps, mapDispatchToProps)(ConnectedSubMenuComps);

export default withStyles(useStyles)(SubMenuComps);