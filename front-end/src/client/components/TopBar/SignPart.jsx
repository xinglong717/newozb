import React from "react";
import { connect } from "react-redux";
import Grid from "@material-ui/core/Grid";
import PropTypes from "prop-types";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import { withStyles } from "@material-ui/core/styles";

import Dialog from '@material-ui/core/Dialog';
import SignIn from '../SignIn';
import SignUp from '../SignUp';


import { openSignInDialog } from '../../../js/actions/Home';
import { closeSignInDialog } from '../../../js/actions/Home';
import { openSignUpDialog } from '../../../js/actions/Home';
import { closeSignUpDialog } from '../../../js/actions/Home';

const useStyles = theme => ({
    root: {
        flexGrow: 1
    },
    mainMenuText: {
        fontWeight: `bold`,
        color: `#000`,
        flexDirection: "column",
        justifyContent: "center",
        "&:hover": {
            color: "#fff"
        }
    },
    navItem: {
        borderRadius: 0,
        border: 0,
        cursor: "pointer",
        "&:hover": {
            background: "#6d4c41",
            color: `#fff`
        },
        "&:after": {
            display: `none`
        },
        backgroundColor: `#f57f17`
    },
});

const mapStateToProps = state => {
    return {
        isUserSigned: state.Home.isUserSigned,
        openSignIn: state.Home.openSignIn,
        openSignUp: state.Home.openSignUp
    };
};
function mapDispatchToProps(dispatch) {
    return {
        openSignInDialog: () => { dispatch(openSignInDialog()) },
        closeSignInDialog: () => { dispatch(closeSignInDialog()) },
        openSignUpDialog: () => { dispatch(openSignUpDialog()) },
        closeSignUpDialog: () => { dispatch(closeSignUpDialog()) }
    };
}

class ConnectedSignPart extends React.Component {
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.onMouseEnter = this.onMouseEnter.bind(this);
        this.onMouseLeave = this.onMouseLeave.bind(this);
        this.state = {
            dropdownOpenSubDealMenu: false,
            dropdownOpenSubForumMenu: false
        };
    }

    toggle(target) {
        if (target === "Deal") {
            this.setState(prevState => ({
                dropdownOpenSubDealMenu: !prevState.dropdownOpenSubDealMenu
            }));
        } else if (target === "Forum") {
            this.setState(prevState => ({
                dropdownOpenSubForumMenu: !prevState.dropdownOpenSubForumMenu
            }));
        }
    }

    onMouseEnter(target) {
        if (target === "Deal") {
            this.setState({ dropdownOpenSubDealMenu: true });
        } else if (target === "Forum") {
            this.setState({ dropdownOpenSubForumMenu: true });
        }
    }

    onMouseLeave(target) {
        if (target === "Deal") {
            this.setState({ dropdownOpenSubDealMenu: false });
        } else if (target === "Forum") {
            this.setState({ dropdownOpenSubForumMenu: false });
        }
    }

    handleSignInClickOpen = () => {
        this.props.openSignInDialog();
    }

    signInHandleClose = () => {
        this.props.closeSignInDialog();
    }

    handleSignUpClickOpen = () => {
        this.props.openSignUpDialog();
    }

    signUpHandleClose = () => {
        this.props.closeSignUpDialog();
    }

    render() {
        const { classes } = this.props;

        return (
            <React.Fragment>
                <Grid container
                    direction="row"
                    justify="flex-start"
                    alignItems="stretch">
                    <Grid item>
                        <Box alignItems="center" onClick={this.handleSignInClickOpen}>
                            <Box p={1} className={classes.navItem}>
                                <Typography
                                    variant="h6"
                                    className={classes.mainMenuText}
                                >
                                    Sign In
                          </Typography>
                            </Box>
                        </Box>
                        <Dialog open={this.props.openSignIn} onClose={this.signInHandleClose} aria-labelledby="form-dialog-title">
                            <SignIn />
                        </Dialog>
                    </Grid>
                    <Grid item>
                        <Box alignItems="center" onClick={this.handleSignUpClickOpen}>
                            <Box p={1} className={classes.navItem}>
                                <Typography
                                    variant="h6"
                                    className={classes.mainMenuText}
                                >
                                    Sign Up
                          </Typography>
                            </Box>
                        </Box>
                        <Dialog open={this.props.openSignUp} onClose={this.signUpHandleClose} aria-labelledby="form-dialog-title">
                            <SignUp />
                        </Dialog>
                    </Grid>
                </Grid>
            </React.Fragment>
        );
    }
}

ConnectedSignPart.propTypes = {
    classes: PropTypes.object.isRequired
};
const SignPart = connect(
    mapStateToProps,
    mapDispatchToProps
)(ConnectedSignPart);
export default withStyles(useStyles)(SignPart);
