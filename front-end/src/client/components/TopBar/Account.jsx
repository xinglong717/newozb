import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import Grid from "@material-ui/core/Grid";
import PropTypes from "prop-types";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";
import SubMenuDeal from "../SubMenuDeal";


import {
    Dropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem    
} from "reactstrap";
import "bootstrap/dist/css/bootstrap.css";

const useStyles = theme => ({
    root: {
        flexGrow: 1
    },
    mainMenuText: {
        fontWeight: `bold`,
        color: `#000`,
        flexDirection: "column",
        justifyContent: "center",
        "&:hover": {
            color: "#fff"
        }
    },
    navItem: {
        borderRadius: 0,
        border: 0,
        cursor: "pointer",
        "&:hover": {
            background: "#6d4c41",
            color: `#fff`
        },
        "&:after": {
            display: `none`
        },
        backgroundColor: `#f57f17`
    }
});

const mapStateToProps = state => {
    return {};
};
function mapDispatchToProps() {
    return {};
}

class ConnectedTopBar extends React.Component {
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.onMouseEnter = this.onMouseEnter.bind(this);
        this.onMouseLeave = this.onMouseLeave.bind(this);
        this.state = {
            dropdownOpenSubDealMenu: false,
        };
    }

    toggle(target) {
        if (target === "Deal") {
            this.setState(prevState => ({
                dropdownOpenSubDealMenu: !prevState.dropdownOpenSubDealMenu
            }));
        }
    }

    onMouseEnter(target) {
        if (target === "Deal") {
            this.setState({ dropdownOpenSubDealMenu: true });
        }
    }

    onMouseLeave(target) {
        if (target === "Deal") {
            this.setState({ dropdownOpenSubDealMenu: false });
        }
    }

    render() {
        const { classes } = this.props;

        return (
            <React.Fragment>
                <Grid
                    direction="row"
                    justify="flex-start"
                    alignItems="stretch"
                >
                    <Grid item>
                        <Dropdown
                            onMouseOver={e => this.onMouseEnter("Deal", e)}
                            onMouseLeave={e => this.onMouseLeave("Deal", e)}
                            isOpen={this.state.dropdownOpenSubDealMenu}
                            toggle={e => this.toggle("Deal", e)}
                        >
                            <Link to={"/deals"} style={{ textDecoration: "none" }}>
                                <DropdownToggle caret className={classes.navItem}>
                                    <Typography
                                        variant="h6"
                                        className={classes.mainMenuText}
                                    >
                                        My Account
                          </Typography>
                                </DropdownToggle>
                            </Link>
                            <DropdownMenu>
                                <DropdownItem header>
                                    <SubMenuDeal />
                                </DropdownItem>
                            </DropdownMenu>
                        </Dropdown>
                    </Grid>
                </Grid>
            </React.Fragment>
        );
    }
}

ConnectedTopBar.propTypes = {
    classes: PropTypes.object.isRequired
};
const TopBar = connect(
    mapStateToProps,
    mapDispatchToProps
)(ConnectedTopBar);
export default withStyles(useStyles)(TopBar);
