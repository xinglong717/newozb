import {
    OPEN_SIGNIN_DIALOG,
    CLOSE_SIGNIN_DIALOG,
    OPEN_SIGNUP_DIALOG,
    CLOSE_SIGNUP_DIALOG,
    USER_SIGNED,
} from "../constants/action-types.js";


function isUserSigned() {
    return { type: USER_SIGNED }
}
function openSignInDialog() {
    return { type: OPEN_SIGNIN_DIALOG }
}
function closeSignInDialog() {
    return { type: CLOSE_SIGNIN_DIALOG }
}

function openSignUpDialog() {
    return { type: OPEN_SIGNUP_DIALOG }
}

function closeSignUpDialog() {
    return { type: CLOSE_SIGNUP_DIALOG }
}

export {
    isUserSigned,
    openSignInDialog,
    closeSignInDialog,
    openSignUpDialog,
    closeSignUpDialog
}
