import {
    USER_SIGNED,
    OPEN_SIGNIN_DIALOG,
    CLOSE_SIGNIN_DIALOG,
    OPEN_SIGNUP_DIALOG,
    CLOSE_SIGNUP_DIALOG
} from "../constants/action-types.js";

    const showDepartState = {
        openSignIn:false,
        openSignUp:false,
        isUserSigned:false,
    };

    function HomeReducer(state = showDepartState, action) {
        
        if (action.type === USER_SIGNED) {
            return  Object.assign({}, state, {
                isUserSigned:true
            }); 
        }
        if (action.type === OPEN_SIGNIN_DIALOG) {
            return  Object.assign({}, state, {
                openSignIn:true
            }); 
        }
        if (action.type === CLOSE_SIGNIN_DIALOG) {
            return  Object.assign({}, state, {
                openSignIn:false
            });
        }

        if (action.type === OPEN_SIGNUP_DIALOG) {
            // console.log("Opened SingUpDialog!")
            return  Object.assign({}, state, {
                openSignUp:true
            });
        }     
        if (action.type === CLOSE_SIGNUP_DIALOG) {
            return  Object.assign({}, state, {
                openSignUp: false
            });
        } 
        
        return state;
    };

  export default HomeReducer;


