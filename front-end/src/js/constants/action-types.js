/**
 * Acton types for Home.jsx
 */

export const USER_SIGNED = "USER_SIGNED";
export const OPEN_SIGNIN_DIALOG = "OPEN_SIGNIN_DIALOG";
export const CLOSE_SIGNIN_DIALOG = "CLOSE_SIGNIN_DIALOG";
export const OPEN_SIGNUP_DIALOG = "OPEN_SIGNUP_DIALOG";
export const CLOSE_SIGNUP_DIALOG = "CLOSE_SIGNUP_DIALOG";



