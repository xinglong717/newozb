import React from 'react';
import { Router, Route, Switch, Redirect } from "react-router-dom";
import ReactDOM from "react-dom";
import { Provider } from 'react-redux';
import store from "./js/store/index";
import { createBrowserHistory } from "history";


import Home from './client/Home';
import Dashboard from './layouts/Dashboard';
import Deals from './client/views/Deals';
import DetailDeal from './client/views/DetailDeal';
import SubmitDeal from './client/views/SubmitDeal';
import Forums from './client/views/Forums';
import Live from './client/views/Live';





var hist = createBrowserHistory();
ReactDOM.render(
  <Provider store={store}>
    <Router history={hist}>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/admin" component={Dashboard} />
        <Redirect from="/admin" to="/admin/dashboard" />
        <Route exact path="/deals" component={Deals} />
        <Route exact path="/live" component={Live} />
        <Route path="/deals/:dealId" component={DetailDeal} />
        <Route path="/category/:categoryId" component={Deals} />
        <Route exact path="/deal/submit" component={SubmitDeal} />
        <Route exact path="/forums" component={Forums} />
      </Switch>
    </Router>
  </Provider>,
  document.getElementById("root")
);
