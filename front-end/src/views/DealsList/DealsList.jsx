import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import Done from "@material-ui/icons/Done";
import Cancel from "@material-ui/icons/Cancel";
import TextField from "@material-ui/core/TextField";
import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";

import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardBody from "components/Card/CardBody.jsx";


const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0"
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF"
    }
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1"
    }
  }
};

const API_URL = 'https://localhost:44380';

class ServiceTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      deals: [{ id: '', title: '', linkedURL: '', couponCode: '', startDate: '',descriptionText:'' }],
      editedIndex: -1,
      editedId: "",
      editedTitle: "",
      editedLinkedURL: "",
      editedCouponCode: "",
      editedStartDate: "",
      editedDescriptionText:"",
    };
  }
  /* 
  * Delete selected row with index in GridTable.
  */
  deleteComponent(index) {
    const deals = this.state.deals.slice();
    const selectedID = deals[index].dealId;
    fetch(`${API_URL}/api/deal/deletedeal?dealId=` + selectedID, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json'
      }
    })
    deals.splice(index, 1);
    this.setState({ deals });
  }


  /*
   * Edit selected row with index in GridTable.
  */

  editComponent(index) {
    this.setState({ editedIndex: index });
    const deals = this.state.deals.slice();
    this.setState({
      editedId: deals[index].dealId,
      editedTitle: deals[index].title,
      editedLinkedURL: deals[index].linkedURL,
      editedCouponCode: deals[index].couponCode,
      editedStartDate: deals[index].startDate,
      editedDescriptionText: deals[index].descriptionText,
    });
  }

  /*
  * Cancel editing
  */

  cancelEdit = () => {// jshint ignore:line
    this.setState({ editedIndex: -1 });// jshint ignore:line
  };// jshint ignore:line

  /*
   * Update selected row with index in GridTable.
   */
  updateComponent(index) {
    const deals = this.state.deals.slice();
    deals[index].dealId = this.state.editedId;
    deals[index].title = this.state.editedTitle;
    deals[index].linkedURL = this.state.editedLinkedURL;
    deals[index].couponCode = this.state.editedCouponCode;
    deals[index].startDate = this.state.editedStartDate;
    deals[index].descriptionText = this.state.editedDescriptionText;
    const data = deals[index];
    fetch(`${API_URL}/api/deal/UpdateDeal`, {
      method: 'PUT', // update
      body: JSON.stringify(data), // data can be `string` or {object}!
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json'
      }
    })
    this.setState({
      deals,
      editedId: "",
      editedTitle: "",
      editedLinkedURL: "",
      editedCouponCode: "",
      editedStartDate: "",
      editedDescriptionText:"",
      editedIndex: -1
    });
  }

  /**
   * Get event in GridTable.
   */
  handleChange = name => event => {
    this.setState({
      [name]: event.target.value
    });
  };

  componentDidMount() {
    fetch(`${API_URL}/api/deal/getdeals`)
      .then(response => response.json())
      .then(response =>
        this.setState({
          deals: response
        })
      )
  }

  render() {
    const { editedIndex, deals } = this.state;
    const { classes } = this.props;
    const deleteIcon = index => (
      <IconButton onClick={() => this.deleteComponent(index)}>
        <DeleteIcon color="secondary" />
      </IconButton>
    );

    const editIcon = index => (
      <IconButton onClick={() => this.editComponent(index)}>
        <EditIcon color="primary" />
      </IconButton>
    );
    const okBtn = index => (
      <IconButton onClick={() => this.updateComponent(index)}>
        <Done color="secondary" />
      </IconButton>
    );
    const cancelBtn = (
      <IconButton onClick={this.cancelEdit}>
        <Cancel color="primary" />
      </IconButton>
    );

    const editTitle = (
      <TextField
        id="title"
        label="Title"
        className={classes.textField}
        value={this.state.editedTitle}
        margin="normal"
        onChange={this.handleChange("editedTitle")}
      />
    );
    const editLinkedURL = (
      <TextField
        id="linkedURL"
        label="LinkedURL"
        className={classes.textField}
        value={this.state.editedLinkedURL}
        margin="normal"
        onChange={this.handleChange("editedLinkedURL")}
      />
    );
    const editCouponCode = (
      <TextField
        id="couponCode"
        label="CouponCode"
        className={classes.textField}
        value={this.state.editedCouponCode}
        margin="normal"
        onChange={this.handleChange("editedCouponCode")}
      />
    );

    const editStartDate = (
      <TextField
        id="editStartDate"
        label="StartDate"
        className={classes.textField}
        value={this.state.editedStartDate}
        margin="normal"
        onChange={this.handleChange("editedStartDate")}
      />
    );
    const editDescriptionText = (
      <TextField
        id="editDescriptionText"
        label="Description"
        className={classes.textField}
        value={this.state.editedDescriptionText}
        margin="normal"
        onChange={this.handleChange("editedDescriptionText")}
      />
    );

    return (
      <GridContainer>
        <GridItem xs={12} sm={12} md={12}>
          <Card plain>
            <CardHeader plain color="success">
              <h4 className={classes.cardTitleWhite}>
                Deals ManageMent
            </h4>
              <p className={classes.cardCategoryWhite}>
                Here is  DealsList
            </p>
            </CardHeader>

            <CardBody>
              <Paper className={classes.root}>
                <Table className={classes.table}>
                  <TableHead>
                    <TableRow>
                      <TableCell>Title</TableCell>
                      <TableCell>LinkedURL</TableCell>
                      <TableCell>CouponCode</TableCell>
                      <TableCell>StartDate</TableCell>
                      <TableCell>EndDate</TableCell>
                      <TableCell>IsFree</TableCell>
                      <TableCell>DescripitonText</TableCell>
                      <TableCell>AmountVotes</TableCell>
                      <TableCell ></TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {deals.map(n => {
                      return (
                        <TableRow key={n.dealId}>
                          <TableCell>
                            {deals.indexOf(n) === editedIndex ? editTitle : n.title}
                          </TableCell>
                          <TableCell>
                            {deals.indexOf(n) === editedIndex ? editLinkedURL : n.linkedURL}
                          </TableCell>
                          <TableCell>
                            {deals.indexOf(n) === editedIndex ? editCouponCode : n.couponCode}
                          </TableCell>
                          <TableCell>
                            {deals.indexOf(n) === editedIndex ? editStartDate : n.startDate}
                          </TableCell>
                          <TableCell>
                            {deals.indexOf(n) === editedIndex ? editStartDate : n.endDate}
                          </TableCell>
                          <TableCell>
                            {deals.indexOf(n) === editedIndex ? editStartDate : n.isFreebie}
                          </TableCell>
                          <TableCell>
                            {deals.indexOf(n) === editedIndex ? editDescriptionText : n.descriptionText}
                          </TableCell>
                          <TableCell>
                            {deals.indexOf(n) === editedIndex ? editStartDate : n.amountVotes}
                          </TableCell>
                          <TableCell className={classes.iconCell} component="th" scope="row" >
                            {deals.indexOf(n) === editedIndex ? cancelBtn : editIcon(deals.indexOf(n))}
                            {deals.indexOf(n) === editedIndex ? okBtn(deals.indexOf(n)) : deleteIcon(deals.indexOf(n))}
                          </TableCell>
                        </TableRow>
                      );
                    })}
                  </TableBody>
                </Table>
              </Paper>
            </CardBody>
          </Card>
        </GridItem>
      </GridContainer>
    );
  }
}
export default withStyles(styles)(ServiceTable);
