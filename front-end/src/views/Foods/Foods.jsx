import React from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import Grid from "@material-ui/core/Grid";
import GridItem from "components/Grid/GridItem.jsx";
// core components
import Button from "components/CustomButtons/Button.jsx";
import Card from "components/Card/Card.jsx";
import CardBody from "components/Card/CardBody.jsx";
import breakfast from "assets/img/foods/breakfast.jpg";
import burgers from "assets/img/foods/burgers.jpg";
import honey from "assets/img/foods/honey.jpg";
import morning from "assets/img/foods/morning.jpg";
import olive from "assets/img/foods/olive.jpg";

import cardImagesStyles from "assets/jss/material-dashboard-react/cardImagesStyles.jsx";

class CardBasic extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <Grid container>
      <GridItem xs={12} sm={12} md={4}>
        <Card style={{ width: "20rem" }}>
            <img
            className={classes.cardImgTop}
            data-src="holder.js/100px180/"
            alt="100%x180"
            style={{ height: "180px", width: "100%", display: "block" }}
            src= {breakfast}
            data-holder-rendered="true"
        />
        <CardBody>
          <h4>Card title</h4>
          <p>
            Some quick example text to build on the card title and make up the
            bulk of the card's content.
          </p>
          <Button color="primary">Go somewhere</Button>
        </CardBody>
        </Card>
        </GridItem>
        <GridItem xs={12} sm={12} md={4}>
       <Card style={{ width: "20rem" }}>
        <img
          className={classes.cardImgTop}
          data-src="holder.js/100px180/"
          alt="100%x180"
          style={{ height: "180px", width: "100%", display: "block" }}
          src= {burgers}
          data-holder-rendered="true"
        />
        <CardBody>
          <h4>Card title</h4>
          <p>
            Some quick example text to build on the card title and make up the
            bulk of the card's content.
          </p>
          <Button color="primary">Go somewhere</Button>
        </CardBody>
      </Card>
     
      </GridItem><GridItem xs={12} sm={12} md={4}>

       <Card style={{ width: "20rem" }}>
        <img
          className={classes.cardImgTop}
          data-src="holder.js/100px180/"
          alt="100%x180"
          style={{ height: "180px", width: "100%", display: "block" }}
          src= {honey}
          data-holder-rendered="true"
        />
        <CardBody>
          <h4>Card title</h4>
          <p>
            Some quick example text to build on the card title and make up the
            bulk of the card's content.
          </p>
          <Button color="primary">Go somewhere</Button>
        </CardBody>
      </Card>
      </GridItem>
      <GridItem xs={12} sm={12} md={4}>
       <Card style={{ width: "20rem" }}>
        <img
          className={classes.cardImgTop}
          data-src="holder.js/100px180/"
          alt="100%x180"
          style={{ height: "180px", width: "100%", display: "block" }}
          src= {breakfast}
          data-holder-rendered="true"
        />
        <CardBody>
          <h4>Card title</h4>
          <p>
            Some quick example text to build on the card title and make up the
            bulk of the card's content.
          </p>
          <Button color="primary">Go somewhere</Button>
        </CardBody>
      </Card>
     
      </GridItem>
      <GridItem xs={12} sm={12} md={4}>
       <Card style={{ width: "20rem" }}>
        <img
          className={classes.cardImgTop}
          data-src="holder.js/100px180/"
          alt="100%x180"
          style={{ height: "180px", width: "100%", display: "block" }}
          src= {olive}
          data-holder-rendered="true"
        />
        <CardBody>
          <h4>Card title</h4>
          <p>
            Some quick example text to build on the card title and make up the
            bulk of the card's content.
          </p>
          <Button color="primary">Go somewhere</Button>
        </CardBody>
      </Card>
     
      </GridItem>
      <GridItem xs={12} sm={12} md={4}>
       <Card style={{ width: "20rem" }}>
        <img
          className={classes.cardImgTop}
          data-src="holder.js/100px180/"
          alt="100%x180"
          style={{ height: "180px", width: "100%", display: "block" }}
          src= {morning}
          data-holder-rendered="true"
        />
        <CardBody>
          <h4>Card title</h4>
          <p>
            Some quick example text to build on the card title and make up the
            bulk of the card's content.
          </p>
          <Button color="primary">Go somewhere</Button>
        </CardBody>
      </Card>
      </GridItem>
    </Grid>
    );
  }
}

export default withStyles(cardImagesStyles)(CardBasic);