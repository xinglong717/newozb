import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import Done from "@material-ui/icons/Done";
import Cancel from "@material-ui/icons/Cancel";
import TextField from "@material-ui/core/TextField";
import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";

import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardBody from "components/Card/CardBody.jsx";
import AddIcon from '@material-ui/icons/Add';


const styles = {
    cardCategoryWhite: {
      "&,& a,& a:hover,& a:focus": {
        color: "rgba(255,255,255,.62)",
        margin: "0",
        fontSize: "14px",
        marginTop: "0",
        marginBottom: "0"
      },
      "& a,& a:hover,& a:focus": {
        color: "#FFFFFF"
      }
    },
    cardTitleWhite: {
      color: "#FFFFFF",
      marginTop: "0px",
      minHeight: "auto",
      fontWeight: "300",
      fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
      marginBottom: "3px",
      textDecoration: "none",
      "& small": {
        color: "#777",
        fontSize: "65%",
        fontWeight: "400",
        lineHeight: "1"
      }
    }
  };

  
class ServiceTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      serviceData:[{id:'',sellerName:'',sex:'',birthday:'',address:''}],
      editedIndex: -1,
      editedId: "",
      editedSellerName: "",
      editedSex:"",
      editedBirthday:"",
      editedAddress:""
    };
  }
  /* 
  * Delete selected row with index in GridTable.
  */
  deleteComponent(index) {
    const serviceData = this.state.serviceData.slice();
    const selectedID = serviceData[index].id
    fetch('https://localhost:5000/api/books/'+ selectedID, {
      method: 'DELETE'
    })
    serviceData.splice(index, 1);
    this.setState({ serviceData });
  }
/**
 * Add new component into ther Table
 */
  addComponent = ()=> {
    console.log("Add button is Clicked!");
    
    const data = {"sellerName":"","sex":"","birthday":"","address":""}
    fetch('https://localhost:5000/api/books/', {
      method: 'POST', // or 'PUT'
      body: JSON.stringify(data), // data can be `string` or {object}!
      headers:{
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json'
      }
    })
    fetch('https://localhost:5000/api/books')
      .then(response => response.json())
      .then(response =>
        this.setState({
          serviceData:response
        })
      )
     
     const serviceData = this.state.serviceData.slice();
    
    const newIndex = serviceData.length+1;
    
    serviceData.push(newIndex);
    this.setState({
      serviceData,
      editedSellerName: "",
      editedSex: "",
      editedBirthday: "",
      editedAddress: "",
      editedIndex: -1
    });
  }

/*
 * Edit selected row with index in GridTable.
*/

  editComponent(index) {
    this.setState({ editedIndex: index });
    const serviceData = this.state.serviceData.slice();
    this.setState({
      editedSellerName:serviceData[index].sellerName,
      editedSex:serviceData[index].sex,
      editedBirthday:serviceData[index].birthday,
      editedAddress:serviceData[index].address
    });
  }

/*
* Cancel editing
*/
  cancelEdit = () => {
    this.setState({ editedIndex: -1 });
  };

/*
 * Update selected row with index in GridTable.
 */
  updateComponent(index) {
    const serviceData = this.state.serviceData.slice();
    serviceData[index].sellerName = this.state.editedSellerName;
    serviceData[index].sex = this.state.editedSex;
    serviceData[index].birthday = this.state.editedBirthday;
    serviceData[index].address = this.state.editedAddress;
    const data = serviceData[index];
    // console.log(JSON.stringify(data));
    fetch('https://localhost:5000/api/books/'+ serviceData[index].id, {
      method: 'PUT', // update
      body: JSON.stringify(data), // data can be `string` or {object}!
      headers:{
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json'
      }
    })
    this.setState({
      serviceData,
      editedId: "",
      editedSellerName: "",
      editedSex: "",
      editedBirthday: "",
      editedAddress: "",
      editedIndex: -1
    });
  }

  /**
   * Get event in GridTable.
   */
  handleChange = name => event => {
    this.setState({
      [name]: event.target.value
    });
  };
/**
 *
 */
  handleSubmit(e) {
    e.preventDefault();
    console.log("Save button is clicked!")
    const data = {"sellerName":"Tomas","sex":211.15,"birthday":"1990-11-11","address":"street1"}
    fetch('https://localhost:5000/api/sellers/', {
      method: 'POST', // or 'PUT'
      body: JSON.stringify(data), // data can be `string` or {object}!
      headers:{
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json'
      }
    })
  }

  
  componentDidMount(){ 
    fetch('https://localhost:5000/api/books')
      .then(response => response.json())
      .then(response =>
        this.setState({
          serviceData:response
        })
      )
  }

  render() {
    const { editedIndex, serviceData } = this.state;
    const { classes } = this.props;
    const deleteIcon = index => (
      <IconButton onClick={() => this.deleteComponent(index)}>
        <DeleteIcon color="secondary" />
      </IconButton>
    );
    
    const editIcon = index => (
      <IconButton onClick={() => this.editComponent(index)}>
        <EditIcon color="primary" />
      </IconButton>
    );
    const okBtn = index => (
      <IconButton onClick={() => this.updateComponent(index)}>
        <Done color="secondary" />
      </IconButton>
    );
    const cancelBtn = (
      <IconButton onClick={this.cancelEdit}>
        <Cancel color="primary" />
      </IconButton>
    );

    const editName = (
      <TextField
        id="name"
        label="Name"
        className={classes.textField}
        value={this.state.editedSellerName}
        margin="normal"
        onChange={this.handleChange("editedSellerName")}
      />
    );
    const editSex = (
      <TextField
        id="sex"
        label="sex"
        className={classes.textField}
        value={this.state.editedSex}
        margin="normal"
        onChange={this.handleChange("editedSex")}
      />
    );
    const editBirthday = (
      <TextField
        id="birthday"
        label="birthday"
        className={classes.textField}
        value={this.state.editedBirthday}
        margin="normal"
        onChange={this.handleChange("editedBirthday")}
      />
    );

    const editAddress = (
      <TextField
        id="address"
        label="address"
        className={classes.textField}
        value={this.state.editedAddress}
        margin="normal"
        onChange={this.handleChange("editedAddress")}
      />
    );

    return (
      <GridContainer>
        <GridItem xs={12} sm={12} md={12}>
        <Card plain>
          <CardHeader plain color="success">
            <h4 className={classes.cardTitleWhite}>
              Seller's Infromation in My Restaurante
            </h4> 
            <p className={classes.cardCategoryWhite}>
              Here is  detail for seller's information
            </p>
           <AddIcon  onClick = {this.addComponent}/> 
            </CardHeader>
        
          <CardBody>
             <Paper className={classes.root}>
                  <Table className={classes.table}>
                    <TableHead>
                      <TableRow>
                        <TableCell>Name</TableCell>
                        <TableCell>Sex</TableCell>
                        <TableCell>Birthday</TableCell>
                        <TableCell>Address</TableCell>
                        <TableCell >Actions</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                    {serviceData.map(n => {
                      return (
                        <TableRow key={n.id}>
                          <TableCell>
                              {serviceData.indexOf(n) === editedIndex ? editName: n.sellerName}
                          </TableCell>
                          <TableCell>
                              {serviceData.indexOf(n) === editedIndex ? editSex : n.sex}
                          </TableCell>
                          <TableCell>
                              {serviceData.indexOf(n) === editedIndex ? editBirthday : n.birthday}
                          </TableCell>
                          <TableCell>
                            
                              {serviceData.indexOf(n) === editedIndex ? editAddress : n.address}
                          </TableCell>
                          <TableCell className={classes.iconCell} component="th" scope="row" >
                            {serviceData.indexOf(n) === editedIndex ? cancelBtn: editIcon(serviceData.indexOf(n))}
                            {serviceData.indexOf(n) === editedIndex ? okBtn(serviceData.indexOf(n)): deleteIcon(serviceData.indexOf(n))}
                          </TableCell>
                        </TableRow>
                      );
                    })}
                </TableBody>
                </Table>
              </Paper>
            </CardBody> 
        </Card>
      </GridItem>
    </GridContainer>
    );
  }
}
export default withStyles(styles)(ServiceTable);
